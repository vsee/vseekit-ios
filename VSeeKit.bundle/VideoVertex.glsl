/**
 * Video view vertex shader
 * Passthrough Shader
 */

attribute vec4 Position;
attribute vec2 TexCoordIn;
varying vec2 TexCoordVarying;


void main(void)
{
    gl_Position = Position;
    TexCoordVarying = TexCoordIn;
}
