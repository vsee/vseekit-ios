/**
 * Video view fragment shader
 * Converts from YUV 4:2:0 to RGB
 */

varying highp vec2 TexCoordVarying;
uniform sampler2D YTexture;
uniform sampler2D UTexture;
uniform sampler2D VTexture;


void main(void)
{
    mediump vec3 yuv;
    lowp vec3 rgb;

    yuv.x = texture2D(YTexture, TexCoordVarying).r;
    yuv.y = texture2D(UTexture, TexCoordVarying).r - 0.5;
    yuv.z = texture2D(VTexture, TexCoordVarying).r - 0.5;

    // BT.601, which is the standard for SDTV is provided as a reference
    /*
    rgb = mat3(    1,       1,     1,
                   0, -.34413, 1.772,
               1.402, -.71414,     0) * yuv;
     */

    // Using BT.709 which is the standard for HDTV
    rgb = mat3(      1,       1,      1,
                     0, -.18732, 1.8556,
               1.57481, -.46813,      0) * yuv;

    gl_FragColor = vec4(rgb, 1);
}
