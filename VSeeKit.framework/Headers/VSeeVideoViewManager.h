/**
 * @file VSeeVideoViewManager.h
 * Manager for local and remote video views
 *
 * @copyright (c) 2012 VSee Lab, Inc. All rights reserved.
 * @ingroup iOSFramework
 */

/**
 * @defgroup iOSFramework VSeeKit iOS Framework
 * @details The VSeeKit Framework allows developers to embed VSee functionality into their iOS Apps.
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol VSeeVideoViewManagerDelegate;

/** Base interface for the singleton view controller of the top level video calling UI
 * @details This class is not suitable for subclassing.
 * @ingroup iOSFramework
 */
@interface VSeeVideoViewManager : UIViewController

/// The singleton video view manager object
+ (instancetype)sharedManager;

/// The video view manager's delegate
@property (weak, nullable, nonatomic) id <VSeeVideoViewManagerDelegate> delegate;
/// True if there is at least one remote video view
@property (readonly, nonatomic, getter = isInVideoCall) BOOL inVideoCall;
/// True if the user is in a call with at least one other user or waiting in a meeting for others to join (NOT OBSERVABLE)
@property (readonly, nonatomic, getter = isInMeeting) BOOL inMeeting;
/// True if app share is being received, whether or not it is currently being displayed
@property (assign, nonatomic, getter = isAppShareActive) BOOL appShareActive;
/// Whether to confirm ending a video call when the hangup button is pressed; default is NO
@property (assign, nonatomic) BOOL confirmEndCall;
/// Whether the back swipe gesture is enabled from the video view manager when on a navigation stack; default is YES
@property (assign, nonatomic) BOOL swipeBackEnabled;
/// View controller to display above other content until the first remote video view is shown; default is nil
@property (strong, nullable, nonatomic) UIViewController *waitForCallViewController;
/// Offset from the center of the screen to apply to the local video view until the first remote video view is shown; default is UIOffsetZero
@property (assign, nonatomic) UIOffset waitForCallOffset;
/// Show the close button before the call starts; default is NO
@property (assign, nonatomic) BOOL showCloseBeforeCall;
/// Show call duration timer while in a call; default is YES
@property (assign, nonatomic) BOOL showCallDurationTimer;
/// Image view for the background of the video view manager default is nil
@property (strong, nullable, nonatomic) UIImageView *backgroundImageView;
/// Border color for each video view when not selected. If nil the default color whose grayscale value is 0.25 and alpha value is 1.0 will be used.
@property (strong, nullable, nonatomic) UIColor *videoBorderColor;
/// Whether to scale local video to fit fullscreen when there is no remote video. If NO, local video will be smaller. Default is YES.
@property (assign, nonatomic) BOOL showLocalVideoFullscreen;
/// Whether system notifications for incoming calls should be anonymized so that the caller's name is not displayed outside the app. Default is NO.
@property (assign, nonatomic) BOOL anonymousNotifications;
/// Full names of remote users
@property (strong, nonatomic) NSArray *remoteUserFullNames;
/// Usernames of remote users
@property (strong, nonatomic) NSArray *remoteUsernames;

/** A toolbar that appears on the left side of the video view manager's top bar
 * @details By default this toolbar is empty but it can be configured to show bar button items specific to the app.
 * @note This toolbar is currently only visible on iPad.
 */
@property (strong, nullable, nonatomic) IBOutlet UIToolbar *leftToolbar;

/** Start a video call
 * @param userNames an array of NSStrings containing the account names of the users to call
 */
- (void)startVideoCallWithUserNames:(NSArray *)userNames;

/** Join a meeting
 * @param meetingName The name of the meeting to join
 * @details This is equivalent to calling `joinMeeting:urlString:hideInfo:` except the default URL string for
 * the meeting will be used and the meeting information will be displayed.
 */
- (void)joinMeeting:(NSString *)meetingName;

/** Join a meeting with custom information
 * @param meetingName The name of the meeting to join.
 * @param urlString The URL to display for a Jitsi web meeting instead of the standard URL.
 * @param hideInfo If true no information will be displayed on how others can join the meeting.
 */
- (void)joinMeeting:(NSString *)meetingName urlString:(NSString *)urlString hideInfo:(BOOL)hideInfo;

/// Override meeting custom information, will affect call info controller.
/// @param meetingName The name of the meeting to override.
/// @param urlString The URL to display for a Jitsi web meeting instead of the standard URL.
/// @param hideInfo If true no information will be displayed on how others can join the meeting.
- (void)setOverrideForMeetingName:(NSString *)meetingName urlString:(NSString *)urlString hideInfo:(BOOL)hideInfo;

/** Determine the number of video views on screen
 * @param numViews the number of active video views current shown, including local video
 * @param numMinimized the number of inactive, audio-only video views currently shown
 */
- (void)numVideoViews:(nullable int *)numViews minimizedCount:(nullable int *)numMinimized;

/** Determine the number of users showing active video
 * @return the number of users that have active video views showing
 * @details Local video is included in the total so this is always greater than or equal to one.
 * A user that has more than one active video stream with aux cams only counts as a single user.
 */
- (NSUInteger)numActiveUsers;

/** Close all video calls
 */
- (void)endVideoCalls;

/** Request the delegate to show the video view manager's view
 */
- (void)showVideo;

/** Request the delegate to show the call survey view
 */
- (void)showCallSurvey:(BOOL)isDevBuild stabilityResult:(NSString *)stabilityTestResult jitsiMeetingName:(nullable NSString *)jitsiMeetingName;

/** Close a video call with a specific user
 * @param accountName VSee account name of the user to end the call with.
 * @details If in a group call, this will remove the other user from the group call for all participants.
 * This has no effect if the local user is not currently in an active call with this person.
 */
- (void)endVideoCallWithUser:(NSString *)accountName;

/** Set the background image for video view
 */
- (void)setBackgroundImage:(nullable UIImage *)image;

/** Add bar button item to right navigation item
 * @param buttonItem new bar button item to add to right navigation item
 * @details When app share is received on the iPhone a right bar button item is automatically added to allow
 * switching between the video views and app share views. This allows adding addition right bar button items.
 * Each new bar button item will appear to the right of the existing bar button items.
 * @note This is only implemented on iPhone and has no effect when used an iPad.
 */
- (void)addRightBarButtonItem:(UIBarButtonItem *)buttonItem;

/** Remove bar button item from right navigation item
 * @param buttonItem bar button item to remove from right navigation item
 * @seealso addRightBarButtonItem:
 * @note This is only implemented on iPhone and has no effect when used an iPad.
 */
- (void)removeRightBarButtonItem:(UIBarButtonItem *)buttonItem;

@end


/** Protocol to inform the delegate that the video view manager has added or removed video views.
 * @details Callbacks to the delegate will always occur on the main thread.
 * @ingroup iOSFramework
 */
@protocol VSeeVideoViewManagerDelegate <NSObject>

@required

/** Show the video view manager
 * @param manager the video view manager to show
 * @details Requests the delegate to show the video view manager as a result of some user action.
 */
- (void)videoViewManagerShowVideo:(VSeeVideoViewManager *)manager;

/** A video view will be shown on screen
 * @param manager the video view manager
 * @param accountName VSee account name of the user whose video view will be shown
 * @param remote true if the video view to show was from a remote client or false if it is local video
 */
- (void)videoViewManager:(VSeeVideoViewManager *)manager willShowUser:(NSString *)accountName isRemote:(BOOL)remote;

/** A remote video view has been removed from the screen
 * @param manager the video view manager
 * @param accountName VSee account name of the user whose video view was removed from the screen
 * @param last true if the video view just shown was the last remote video view
 */
- (void)videoViewManager:(VSeeVideoViewManager *)manager didRemoveUser:(NSString *)accountName isLastRemote:(BOOL)last;

@optional

/** The local user declined an incoming video call
 * @param manager the video view manager
 * @param accountName VSee account name of the caller who was declined
 * @details This method is only called when the local user explicitly declines an incoming video call.
 * If a video call is not answered because the user does not accept the call in time, this method is
 * not called.
 */
- (void)videoViewManager:(VSeeVideoViewManager *)manager declinedCallFromUser:(NSString *)accountName;

/** The local user ended a video call
 * @param manager the video view manager
 * @param accountNamesArray an array of the VSee account names of the users who were in the call
 * @details This method is only called when explicitly leaving a video call by calling endVideoCalls or
 * by the local user hanging up in the UI. If a call is terminated the because the remote users hang up,
 * this method is not called.
 */
- (void)videoViewManager:(VSeeVideoViewManager *)manager endedCallWithUsers:(NSArray *)accountNamesArray;

/** Show the call survey
* @param manager the video view manager
* @details This method is called periodically at the end of calls to provide a survey form for the user
* to give feedback on the quality of the call. This feedback can be matched with metrics measured during
* the call to gather useful diagnostics. If you are not interested in gathering call survey data, you
* may ignore or not implement this method.
*/
- (void)videoViewManager:(VSeeVideoViewManager *)manager showCallSurvey:(UIViewController *)viewcontroller;

@end

NS_ASSUME_NONNULL_END
