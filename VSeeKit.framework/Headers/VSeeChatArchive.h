/**
 * @file VSeeChatArchive.h
 * Singleton that gives access to the local and server side chat archive
 *
 * @copyright (c) 2017-2022 VSee Lab, Inc. All rights reserved.
 * @ingroup VSeeKitCore
 */

#import <Foundation/Foundation.h>
#import "VSeeChatMessage.h"

NS_ASSUME_NONNULL_BEGIN

/** Type definition for completion block that is executed when retrieving a list of chat messages completes
 * @param requestId The request id.
 * @param messages An array of chat messages ordered oldest to newest retrieved by the request or nil if
 * there were no results.
 * @param errorString A string describing the error if an error occured, otherwise nil.
 * @note The completion block will generally not be called on the main thread. Chat messages are received
 * through various pathways including directly from the server, delayed delivery while offline, or loaded
 * from the chat archive. Thus, received messages may be duplicated or delivered out of order. The chat message
 * id should always be used to check for uniqueness.
 */
typedef void (^GetChatComplete)(int requestId, NSArray <VSeeChatMessage *> * _Nullable messages, NSString * _Nullable errorString);

/** Type definition for completion block that is executed when retrieving a list of chat messages completes
 * @param requestId The request id.
 * @param messages An array of chat messages ordered oldest to newest retrieved by the request or nil if
 * there were no results.
 * @param purgeBefore Indicates if any existing messages before this timestamp displayed in the UI should be removed before the new
 * results are added. This can occur if there is a gap detected in the chat history.
 * @param errorString A string describing the error if an error occured, otherwise nil.
 * @note The completion block will generally not be called on the main thread. Chat messages are received
 * through various pathways including directly from the server, delayed delivery while offline, or loaded
 * from the chat archive. Thus, received messages may be duplicated or delivered out of order. The chat message
 * id should always be used to check for uniqueness.
 */
typedef void (^GetChatCompletePurge)(int requestId, NSArray <VSeeChatMessage *> * _Nullable messages, uint64_t purgeBefore, NSString * _Nullable errorString);

/** Type definition for completion block that is executed when retrieving a list of chat markers completes
 * @param requestId The request id.
 * @param markers An array of chat markers ordered oldest to newest retrieved by the request or nil if
 * there were no results.
 * @param errorString A string describing the error if an error occured, otherwise nil.
 * @note The completion block will generally not be called on the main thread.
 */
typedef void (^GetMarkersComplete)(int requestId, NSArray <VSeeChatMarker *> * _Nullable markers, NSString * _Nullable errorString);

/** Type definition for completion block that is executed after deleting all chat with a remote user
 * @param chatId The account name or room name whose messages should be deleted.
 * @param errorString A string describing the error if an error occurred, otherwise nil.
 * @note The completion block will generally not be called on the main thread.
 */
typedef void (^DeleteChatComplete)(NSString * _Nonnull chatId, NSString * _Nullable errorString);

// Keys in the dictionary returned by recentChatList
/// Value for this key is the account name or room name of a recent chat.
extern NSString * const kVSeeChatIdKey;
/// Value for this key is an NSDate containing the time of last chat activity in a recent chat.
extern NSString * const kVSeeLastChatTimeKey;
/// Value for this key is an NSNumber indicating whether a recent chat is a group chat.
extern NSString * const kVSeeIsGroupChatKey;

/** Singleton object that gives access to the local and server side chat archive
 *
 * @ingroup VSeeKitCore
 */
@interface VSeeChatArchive : NSObject

@property (assign, getter=isSynced, atomic) BOOL synced;    ///< Whether the chat archive has been synced with the XMPP server for 1-1 messages
@property (assign, getter=isSyncingRecentChat, atomic) BOOL syncingRecentChat;    ///< Whether the chat archive is syncing with the XMPP server for recent messages

/// The singleton chat archive object
+ (instancetype)sharedArchive;

/** Sync with XMPP server and check for 1-1 chat messages not in local chat history
 * @param completeBlock The block to execute when the request is complete or fails.
 * @details You will typically not need to call this method as it is called automatically when logging in
 * or reconnecting to the server after the app returns to the foreground.
 */
- (void)syncChatMessagesCompletion:(GetChatCompletePurge)completeBlock;

/// Get chat messages to or from a user before a timestamp
/// @param vseeId The account name of user's messages to retrieve.
/// @param utcTime Time in milliseconds since epoch. If utcTime is zero the latest messages will be fetched. Otherwise messages before utcTime will be returned.
/// @param nMax The max number of messages to return.
/// @param completeBlock The block to execute when the request is complete or fails.
/// @details This function returns the chat history retrieved from the local DB only.
- (void)queryLocalChatMessagesForUser:(NSString *)vseeId before:(uint64_t)utcTime count:(NSInteger)nMax completion:(GetChatComplete)completeBlock;

/** Get chat messages to or from a user before a timestamp
 * @param vseeId The account name of user's messages to retrieve.
 * @param nMax The max number of messages to return.
 * @param utcTime Time in milliseconds since epoch. If utcTime is zero the latest messages will be fetched. Otherwise messages before utcTime will be returned.
 * @param completeBlock The block to execute when the request is complete or fails.
 * @details This function returns the chat history retrieved from the XMPP server.
 */
- (void)queryChatMessagesForUser:(NSString *)vseeId before:(uint64_t)utcTime count:(NSInteger)nMax completion:(GetChatComplete)completeBlock;

/// Get recent chat messages for a room
/// @param roomName The name of the chat room to retreive messages for.
/// @param nMax The max number of messages to return from local DB WHEN FAILED TO SYNC.
/// @param completeBlock The block to execute when the request is complete or fails.
/// @details This function returns 1 week worth of chat history retrieved from the XMPP server. Call this on room startup/doSync when MucSub is disabled (or disableMucSyncing is true)
- (void)queryRecentChatMessagesForRoom:(NSString *)roomName count:(NSInteger)nMax completion:(GetChatCompletePurge)completeBlock;

/// Get chat messages for a chat room before a timestamp
/// @param roomName The name of the chat room to retreive messages for.
/// @param utcTime Time in milliseconds since epoch. If utcTime is zero the latest messages will be fetched. Otherwise messages before utcTime will be returned.
/// @param nMax The max number of messages to return.
/// @param completeBlock The block to execute when the request is complete or fails.
/// @details This function returns the chat history retrieved from the local DB only.
- (void)queryLocalChatMessagesForRoom:(NSString *)roomName before:(uint64_t)utcTime count:(NSInteger)nMax completion:(GetChatComplete)completeBlock;

/** Get chat messages for a chat room before a timestamp
 * @param roomName The name of the chat room to retreive messages for.
 * @param nMax The max number of messages to return.
 * @param utcTime Time in milliseconds since epoch. If utcTime is zero the latest messages will be fetched. Otherwise messages before utcTime will be returned.
 * @param completeBlock The block to execute when the request is complete or fails.
 * @details This function returns the chat history retrieved from the XMPP server.
 */
- (void)queryChatMessagesForRoom:(NSString *)roomName before:(uint64_t)utcTime count:(NSInteger)nMax completion:(GetChatComplete)completeBlock;

/// Retrieve chat markers for a chat room from the local DB only.
/// @param roomName The name of the chat room to retreive chat markers for.
/// @param utcTimeA Start time of the query in milliseconds since epoch.
/// @param utcTimeB End time of the query in milliseconds since epoch.
/// @param completeBlock The block to execute when the request is complete or fails.
- (void)queryLocalChatMarkersForRoom:(NSString *)roomName start:(uint64_t)utcTimeA end:(uint64_t)utcTimeB completion:(GetMarkersComplete)completeBlock;

/**
 * Retrieve chat markers for a chat room from XMPP server.
 * @param roomName The name of the chat room to retreive chat markers for.
 * @param utcTimeA Start time of the query in milliseconds since epoch.
 * @param utcTimeB End time of the query in milliseconds since epoch.
 * @param completeBlock The block to execute when the request is complete or fails.
 */
- (void)queryChatMarkersForRoom:(NSString *)roomName start:(uint64_t)utcTimeA end:(uint64_t)utcTimeB completion:(GetMarkersComplete)completeBlock;

/** Delete all 1-1 chat messages to and from a user
 * @param vseeId The account name of user's messages to delete
 */
- (void)deleteChatMessagesForUser:(NSString *)vseeId;

/** Delete all 1-1 chat messages to and from a user
 * @param vseeId The account name of user's messages to delete.
 * @param completeBlock The block to execute when the deletion is completed or fails.
 */
- (void)deleteChatMessagesForUser:(NSString *)vseeId completion:(DeleteChatComplete)completeBlock;

/** Delete all chat messages in a chat room
 * @param roomName The name of the chat room whose messages should be deleted.
 * @param completeBlock The block to execute when the deletion is completed or fails.
 * @details This currently only deletes the messages stored in the local database.
 */
- (void)deleteChatMessagesForRoom:(NSString *)roomName completion:(DeleteChatComplete)completeBlock;

/** Mark all messages read from a remote user or group chat
 * @param chatId The account name or room name whose messages should be marked as all read.
 * @details This only affects the local database so only the messages which have currently been received are
 * marked as read.
 */
- (void)markMessagesReadForChat:(NSString *)chatId;

/** Mark all messages read from all users and group chat rooms
 * @details This only affects the local database so only the messages which have currently been received are
 * marked as read.
 */
- (void)markAllMessagesRead;

/** Return the unread message count from a remote user or group chat
 * @param chatId The account name or room name for the count.
 * @return The unread message count from the remote user or group chat.
 */
- (NSUInteger)unreadMessageCountForChat:(NSString *)chatId;

/** Return the total unread message count from all users and group chat rooms
 * @return The total unread message count from all users and group chat rooms.
 */
- (NSUInteger)totalUnreadMessageCount;

/** Return all priority messages from a user that have not expired or been acknowledged
 * @param vseeId The account name of the remote user.
 * @return An array of active priority chat messages.
 */
- (NSArray <VSeeOneToOneChatMessage *> *)activePriorityMessagesForUser:(NSString *)vseeId;

/** Return all waiting room priority messages that have not expired or been acknowledged
 * @return An array of active priority waiting room chat messages.
 */
- (NSArray <VSeeWaitingRoomChatMessage *> *)activeWaitingRoomPriorityMessages;

/** Acknowledge all priority messages from a user
 * @param vseeId The account name of the remote user.
 * @note Per the XEP-0333 spec, "Clients MUST NOT mark a message as acknowledged without any user interaction."
 */
- (void)acknowledgePriorityMessagesForUser:(NSString *)vseeId;

/** Acknowledge all priority waiting room messages
 * @note Per the XEP-0333 spec, "Clients MUST NOT mark a message as acknowledged without any user interaction."
 */
- (void)acknowledgeWaitingRoomPriorityMessages;

/** Acknowledge all priority messages for Clinic dashboard
 */
- (void)acknowledgeClinicPriorityMessages;

/** Check if a priority message has been acknowledged
 * @param messageId of the message to check
 * @return YES if message is acknowledged, otherwise NO
 * @details Extra push notifications for priority messages can sometimes be sent after the priority message has been
 * acknowledged remotely. This method should be used to check when a push notification is received to determine whether
 * the notification should be displayed.
 */
- (BOOL)isPriorityMessageAcknowledged:(NSString *)messageId;

/** Last "displayed" chat marker received from a remote user
 * @param vseeId The account name of the remote user.
 * @return The chat marker
 */
- (VSeeChatMarker *)lastDisplayedChatMarkerForUser:(NSString *)vseeId;

/** Last "acknowledged" chat marker received from a remote user
 * @param vseeId The account name of the remote user.
 * @return The chat marker
 */
- (VSeeChatMarker *)lastAcknowledgedChatMarkerForUser:(NSString *)vseeId;

/** Set chat as current
 * @param chatId The account name or room name whose chat is current or nil if no chat is current.
 * @param isGroup Is the chatId reference a group chat?
 * @details The current chat is used for two purposes:
 *  - Sending the "displayed" chat marker.
 *  - Notifying the group chat service which is the current group chat for joining/leaving chat rooms.
 *
 * If the chat history is in sync with the server and the current chat is visible (See `setCurrentChatVisible:`.) this
 * method will automatically send a "displayed" chat marker if necessary. Otherwise a "displayed" chat marker will be
 * sent as needed after the server sync has been completed and/or the current chat is marked visible.
 *
 * Only one chat id can be current at a time, so making one chat current makes the previous one no longer current.
 * The current chat should not be reset to nil just because the current chat is no longer visible in the UI. Once the
 * current chat changes the user will leave the chat room of the previous current chat. The current chat should only
 * be changed or reset once the user is really done with the chat.
 */
- (void)setCurrentChat:(NSString *)chatId isGroup:(BOOL)isGroup;

/** Set the current chat as being visible or not
 * @details The visibility of the current chat is used for the purposes of sending the "displayed" chat marker.
 * If the chat history is in sync with the server and the current chat is visible this method will automatically
 * send a "displayed" chat marker if necessary. Otherwise the "displayed" chat marker will be sent after the server
 * sync has been completed and/or the current chat is marked visible. The assumption is that chat from only
 * @note It is okay to set the same visibility multiple times. The common code will only take action when the
 * visibility changes.
 * @param isVisible Set the current chat is visible or not.
 */
- (void)setCurrentChatVisible:(BOOL)isVisible;

/** Get array of all recent chats
 * @return An array of dictionaries that describe all the recent chats. Each dictionary will contain the keys
 * kVSeeChatIdKey, kVSeeLastChatTimeKey, and kVSeeIsGroupChatKey.
 * @details A chat with a user or in a chat room is considered recent once a message is sent or received until the
 * chat is explicitly removed from the recent list.
 */
- (NSArray <NSDictionary *> *)recentChatList;

/** Add a chat to the recent chat list and remove it from the hidden chat list
 * @param chatId The account name or room name of the chat to add to the list.
 * @return YES if the chat is currently hidden or NO otherwise.
 * @details The chat must be a member of the hidden chat list or this function will have no effect.
 */
- (BOOL)addRecentChat:(NSString *)chatId;

/** Add a chat to the recent chat list
 * @param chatId The account name or room name of the chat to add to the list.
 * @param date The most recent time the chat has been active.
 * @param isGroup YES if the chat is a group chat or NO for 1-1 chat.
 * @details The chat is removed from the hidden chat list if it is present there.
 */
- (void)addRecentChat:(NSString *)chatId date:(NSDate *)date isGroup:(BOOL)isGroup;

/** Remove chat from the recent chat list and add it to the hidden chat list
 * @param chatId The account name or room name of the chat to remove from the list.
 */
- (void)removeRecentChat:(NSString *)chatId;

/** Get array of all hidden chats
 * @return An array of dictionaries that describe all the hidden chats. Each dictionary will contain the keys
 * kVSeeChatIdKey, kVSeeLastChatTimeKey, and kVSeeIsGroupChatKey.
 * @details A group chat room is considered hidden when it is explicitly removed from the recentChatList. It will stay
 * hidden until a new message is received in that chat room.
 */
- (NSArray <NSDictionary *> *)hiddenChatList;

/** Get array of all past and present members of a chat room
 * @param roomName The name of the chat room.
 * @return An array of the account names of all past and present members of a chat room, excluding the local user.
 */
- (NSArray <NSString *> *)getAllMembersForRoom:(NSString *)roomName;

/** Return the date that messages were last synced with the server for a chat
 * @param chatId The account name or room name of the chat.
 * @return The data of the last server sync for a chat.
 */
- (NSDate *)lastSyncDateForChat:(NSString *)chatId isGroup:(BOOL)isGroup;

@end

NS_ASSUME_NONNULL_END
