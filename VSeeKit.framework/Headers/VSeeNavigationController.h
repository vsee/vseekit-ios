/**
 * @file VSeeNavigationController.h
 * Subclass of UINavigationController that is safe to queue multiple animated transitions on
 *
 * @details It is not safe to animate a transition on UINavigationController when another animated transition is in
 * progress. Doing so can leave the view controller stack in an inconsistent state and lead to UI issues and crashes.
 * VSeeNavigationController is a safe subclass of UINavigationController that can have multiple animated transitions
 * queued at the same time. The transitions are executed in the order they are received, one after another, once the
 * previous transition has completed.
 *
 * @copyright (c) 2018 VSee Lab, Inc. All rights reserved.
 * @ingroup iOSFramework
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/** Subclass of UINavigationController that is safe to queue multiple animated transitions on
 *
 * @ingroup iOSFramework
 */
@interface VSeeNavigationController : UINavigationController <UINavigationControllerDelegate>

/** Run a block of code that contains an animated transition
 * @param codeBlock The block of code to execute.
 * @details The code block containing a transition is put onto the queue for animated transitions. This allows for
 * additional checks on the state of the navigation stack at the time the animation is actually happening, rather
 * than when the animation is queued.
 * @note Only one transition should be animated within the block.
 */
- (void)pushCodeBlock:(void (^)(void))codeBlock;

/** Add a gesture recognizer for swiping on the right edge
 * @param target The object to call when swiping on the right edge.
 * @param selector The selector to call when swiping on the right edge.
 */
- (void)addSwipeRightEdgeGestureWithTarget:(id)target selector:(SEL)selector;

/* Clear the gesture recognizer for swiping on the right edge
 */
- (void)clearSwipeRightEdgeGesture;

@end

NS_ASSUME_NONNULL_END
