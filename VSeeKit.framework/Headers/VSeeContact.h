/**
 * @file VSeeContact.h
 * Object containing information about a contact in the address book
 *
 * @copyright (c) 2015 VSee Lab, Inc. All rights reserved.
 * @ingroup iOSFramework
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/** @enum VSeePresenceStatus
 * These are various types of presence status for contacts
 * @ingroup iOSFramework
 */
typedef NS_ENUM(NSInteger, VSeePresenceStatus) {
    VSeePresenceStatusLogout,                       /*!< offline: logged out */
    VSeePresenceStatusLogin,                        /*!< online: logged in */
    
    // These options are automatically set and sent by client
    VSeePresenceStatusIdle,                         /*!< online: idle */
    VSeePresenceStatusMobile,                       /*!< online: mobile with push enabled */
    VSeePresenceStatusOnCall,                       /*!< online: on another call */

    // These options are set manually by local users
    VSeePresenceStatusInvisible,                    /*!< online: appear as if offline */
    VSeePresenceStatusAway,                         /*!< online: appear as if idle */
    VSeePresenceStatusBusy,                         /*!< online: appear as if on call */

    VSeePresenceStatusUnregistered = 11,            /*!< offline: user has not completed registration yet */
    VSeePresenceStatusInvalid = 200                 /*!< offline: invalid status */
};


/**
 * Contact object
 *
 * @details This object contains information about a contact in the address book.
 * @ingroup iOSFramework
 */
@interface VSeeContact : NSObject <NSCoding>

@property (readonly, nonatomic) NSString *name;     ///< Contact's full name if available or the account name if not
@property (copy, nonatomic) NSString *userName;     ///< VSee account name of the contact
@property (copy, nonatomic) NSString *server;       ///< XMPP server name of the contact
@property (copy, nonatomic) NSString *firstName;    ///< First name of the contact
@property (copy, nonatomic) NSString *lastName;     ///< Last name of the contact
@property (weak, readonly, nonatomic) NSString *fullName;   ///< First and last name of the contact
@property (assign, nonatomic) BOOL subscribed;      ///< Has the local user successfully subscribed to the user's presence?
@property (assign, nonatomic) VSeePresenceStatus status;    ///< Presence status of the contact
@property (readonly, nonatomic) BOOL online;                ///< True if the presence status is one of the online statuses
@property (copy, nonatomic) NSString *userType;     ///< Type of user for the contact. This is used internally by VSeeKit.
@property (readonly, nonatomic) BOOL isApiUser;     ///< This will be true for users created through the VSee WebAPI
@property (assign, nonatomic) BOOL temporary;       ///< Temporary contacts are not persisted to the server and their status is not updated
@property (assign, nonatomic) BOOL isTempStatusReliable;  ///< Status of temp contact is reliable

@end


/**
 * Subscription request object
 *
 * @details This object contains information on an incoming subscription request.
 * @ingroup iOSFramework
 */
@interface VSeeSubscriptionRequest : NSObject
@property (copy, nonatomic) NSString *accountName;  ///< VSee account name of the remote user who made the request
@property (copy, nonatomic) NSString *server;       ///< XMPP server name of the remote users who made the request
@property (copy, nonatomic) NSString *displayName;  ///< The full name of the remote user suitable for display
@property (copy, nonatomic) NSString *message;      ///< The message provided by the remote user accompanying the request
@end

NS_ASSUME_NONNULL_END
