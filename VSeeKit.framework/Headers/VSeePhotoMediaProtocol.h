/**
 * @file VSeePhotoMediaProtocol.h
 * Protocol for photo media model objects that represent a single photo attachment for chat messages
 *
 * @copyright (c) 2017 VSee Lab, Inc. All rights reserved.
 * @ingroup VSeeKitCore
 */

#import <TargetConditionals.h>
#if TARGET_OS_OSX
#import <Cocoa/Cocoa.h>
typedef NSImage VSeeChatImage;
#else
#import <UIKit/UIKit.h>
typedef UIImage VSeeChatImage;
#endif

/** The types of download or upload statuses for media model objects
 */
typedef NS_ENUM(NSInteger, VSeeMediaStatus)  {
    VSeeMediaStatusInit,            ///< No status, initializing media object
    VSeeMediaStatusUploading,       ///< Media data is uploading to the server
    VSeeMediaStatusDownloading,     ///< Media data is downloading from the server
    VSeeMediaStatusCompleted,       ///< All media data is available locally
    VSeeMediaStatusIncomplete       ///< A partial representation of the media data is available
};

/** Protocol for photo media model objects that represent a single photo attachment for chat messages
 */
@protocol VSeePhotoMediaProtocol <NSObject>

@property (strong, nullable, nonatomic) VSeeChatImage *image;           ///< The image data
@property (strong, nullable, nonatomic) VSeeChatImage *thumbnailImage;  ///< A thumbnail representation of the image data
@property (copy, nullable, nonatomic) NSString *imageUrl;   ///< The base URL the image can be downloaded from
@property (copy, nonnull, nonatomic) NSString *imageAlt;    ///< The alternative text to display if unable to display the image
@property (assign, nonatomic) VSeeMediaStatus status;       ///< The download or upload status of the image data
@property (readonly, nonatomic) BOOL incoming;              ///< True if the image data was sent originally from a remote user

#if TARGET_OS_IOS
@property (readonly, nonatomic) BOOL isMediaCached;         ///< Whether the media data for this message is available in the cache

/**
 *  @return An initialized view object that represents the data for this media object.
 *  @discussion The object will return `nil` from this method while the media data is being downloaded.
 */
- (nullable UIView *)mediaView;
#endif

@end
