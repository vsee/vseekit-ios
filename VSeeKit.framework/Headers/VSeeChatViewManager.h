/**
 * @file VSeeChatViewManager.h
 * Base interface for the singleton view controller of the top level chat UI
 *
 * @copyright (c) 2017 VSee Lab, Inc. All rights reserved.
 * @ingroup iOSFramework
 */

#import <UIKit/UIKit.h>
#import "VSeeChatService.h"

NS_ASSUME_NONNULL_BEGIN

@protocol VSeeChatViewManagerDelegate, VSeeChatViewControllerProtocol;
typedef UIViewController <VSeeChatViewControllerProtocol> VSeeChatController;

/** Singleton view controller for the top level chat UI
 *
 * @ingroup iOSFramework
 */
@interface VSeeChatViewManager : UIViewController <VSeeChatReceiveDelegate>

/// The singleton chat view manager object
+ (instancetype)sharedManager;

@property (weak, nullable, nonatomic) id<VSeeChatViewManagerDelegate> delegate;             ///< chat view manager's delegate

/** Array of all recent chat view controllers
 * @details This includes recent chat view controllers of all types that should be displayed in the UI. */
@property (strong, readonly, nonatomic) NSArray <VSeeChatController *> *chatControllers;

/** Array of all hidden chats
 * @details This includes any chats that have been explicitly hidden. If new messages are received by a hidden chat the chat
 * view controller representing it will automatically be created as needed and moved to the active chatControllers.
 * @note The members of this set are not necessarily chat view controllers. */
@property (strong, readonly, nonatomic) NSArray <id<VSeeChatViewControllerProtocol> > *hiddenChats;

/** Number of chat view controllers
 * @details This is equal to the count of chatControllers.
 * @deprecated Use chatControllers.count instead. */
@property (assign, readonly, nonatomic) NSUInteger chatCount __attribute__((deprecated("Use chatControllers.count instead")));

@property (assign, nonatomic) int badgeCount;                                               ///< Number of unread chat messages in all chats

/** Anonymize system notifications
 * @details This controls whether system notifications for incoming chats should be anonymized so that the sender's name is not displayed
 * outside the app. The default is YES.
 * @note This option currently has no effect with push notifications, which are always anonymized. */
@property (assign, nonatomic) BOOL anonymousNotifications;

/** Restore recent chats from previous login sessions
 * @return YES if any chats were restored
 * @details This method determines which chats the user has participated in recently either on this or other devices.
 * A chat view controller is created for each recent chat if it does not exist already.
 */
- (BOOL)restoreRecentChats;

/** Show chat view controller
 * @param chatController the chat view controller to show or nil to show the current active chat controller
 */
- (void)showChatController:(nullable VSeeChatController *)chatController;

/** Close chat view controller
 * @param chatController the chat view controller to close
 * @details This removes the chat controller from the list of chatControllers so it is no longer potentially visible.
 * For group chat controllers this method leaves the group chat and then adds it to the hiddenChats list.
 */
- (void)closeChatController:(VSeeChatController *)chatController;

/** Delete archived chat view controller
 * @param chatController the chat view controller to delete
 * @details This closes the chat controllers but additionally deletes all chat history associated with the chat controller.
 * For group chat controllers this also removes the group chat from the hiddenChats list so there is no more record of it.
 */
- (void)deleteArchivedChatController:(VSeeChatController *)chatController;

/** Delete  chat view controller
 * @param chatController the chat view controller to delete
 * @details This closes the chat controllers but additionally deletes all chat history associated with the chat controller.
 * For group chat controllers this also removes the group chat from the hiddenChats list so there is no more record of it.
 */
- (void)deleteChatController:(VSeeChatController *)chatController;

/** Test if a chat view controller is visible
 * @param chatController the chat view controller
 * @details A chat view controller is only visible if it is the one being shown by the chat view manager
 * and chats are being shown in the UI. */
- (BOOL)isChatControllerVisible:(VSeeChatController *)chatController;

/** Show 1-1 chat
 * @param userName account name of user to chat with
 * @details A new 1-1 chat view controller is created if there is not already one for chatting with this user.
 * If userName is nil the chat view manager is shown without any changes.
 */
- (void)showOneToOneChatWithUserName:(nullable NSString *)userName;

/** Find 1-1 chat view controller
 * @param userName account name of the user to chat with
 * @return The chat view controller for chatting with this user is returned if it exists. Otherwise returns nil.
 */
- (nullable VSeeChatController *)chatControllerForUserName:(NSString *)userName;

/** Find 1-1 chat view controller or create chat view controller if it not exist
* @param userName account name of the user to chat with
* @return The chat view controller for chatting with this user is.
*/
- (VSeeChatController *)getChatControllerForUserName:(NSString *)userName;

/// Find group chat that has the same participants
/// Return nil if not exist
/// @param userNames participants account name
- (VSeeChatController *)chatControllerForUserNames:(NSArray<NSString *>*)userNames;
- (NSString *)chatControllerRoomNameForUserNames:(NSArray<NSString *>*)userNames;

/** Show group chat for room name
 * @param roomName the name of the chat room for this group chat
 * @details A new group chat view controller is created if there is not already one with this room name.
 */
- (void)showGroupChatWithRoomName:(NSString *)roomName;

/** Show group chat for chatting with users
 * @param userNames an array of account names to invite to the group chat
 * @return the name of the chat room created for this group chat
 * @details A new group chat view controller and chat room is created even if there is already one with this list of participants.
 */
- (NSString *)showGroupChatWithUserNames:(NSArray *)userNames displayName:(NSString *)displayName;

/** Find group chat controller for a room name
 * @param roomName the name of the chat room for the group chat
 * @return The chat view controller for this group chat room is returned if it exists. Otherwise returns nil.
 */
- (nullable VSeeChatController *)chatControllerForRoomName:(NSString *)roomName;

/** Show in call chat
 * @details The in call chat view controller is created if it does not already exist.
 */
- (void)showInCallChat;

/** Create in call chat
 * @details Creates the in call chat view controller, but does not show it as the active chat view.
 */
- (void)createInCallChat;

/** Close a 1-1 chat
 * @param userName account name of user to close chat for
 */
- (void)closeOneToOneChatWithUserName:(NSString *)userName;

/** Hide a group chat
 * @param roomName the name of the chat room to hide
 * @details Hiding a group chat will remove it from chatControllers and add it to hiddenChats so it is hidden in the UI without leaving
 * the chat room. The group chat controller will automatically be added back to chatControllers if and when a new message is received
 * in the group.
 */
- (void)hideGroupChatWithRoomName:(NSString *)roomName;

/** Unhide a group chat
 * @param roomName the name of the chat room to unhide
 * @details This removes a group chat from hiddenChats and moves it to chatControllers so it can be shown again in the UI.
 */
- (void)unhideGroupChatWithRoomName:(NSString *)roomName;

/** Determine if a group chat is hidden
 * @parm roomName the name of the chat room
 * @return YES if the group chat room is hidden; NO if the chat room is not hidden or does not exist. */
- (BOOL)isGroupChatHiddenWithRoomName:(NSString *)roomName;

/** Leave a group chat room
 * @param roomName the name of the chat room to leave
 * @details Leaving a group chat room will remove the user from the chat room without hiding the group chat in the UI.
 * The local user can still view the archived chat history but can not send or receive messages in the chat room.
 * @return YES if the leaving group chat successfully; NO if it require assigning new owner before leaving or room not exist. */
- (BOOL)leaveGroupChatWithRoomName:(NSString *)roomName;

/** Leave and hide a group chat
 * @param roomName the name of the chat room to close
 * @details This convenience function is equivalent to calling leaveGroupChatWithRoomName: followed by hideGroupChatWithRoomName:.
 */
- (void)closeGroupChatWithRoomName:(NSString *)roomName;

/** Close in call chat
 */
- (void)closeInCallChat;

/** Close all chats
 * @details This will also cause the user to leave all group chats.
 */
- (void)closeAllChats;

/** Reset the unread chat count for a chat view controller
 * @param chatController The chat view controller
 * @details This method sets the chat controller's `unreadChatCount` property to zero and marks all messages as read in the local
 * database. This method also recalculates the chat view manager's `badgeCount`.
 */
- (void)resetUnreadCountForChatController:(UIViewController <VSeeChatViewControllerProtocol> *)chatController;

/** The users currently in the in-call / meeting chat
 * @return An array containing the account names of all the users in the current call.
 */
- (NSArray <NSString *> *)meetingParticipants;

@end


/** Protocol to inform the delegate of the creation or destruction of chat views
 * @ingroup iOSFramework
 */
@protocol VSeeChatViewManagerDelegate <NSObject>

/** Hide the chat view manager
 * @param manager the chat view manager to hide
 */
- (void)chatViewManagerHideChat:(VSeeChatViewManager *)manager;

/** Show the chat view manager
 * @param manager the chat view manager to show
 */
- (void)chatViewManagerShowChat:(VSeeChatViewManager *)manager;

@optional

/** A chat view controller was added to the recent chat controllers
 * @param manager The chat view manager.
 * @param chatController The chat controller for the new chat that was added.
 * @param first YES if the chat controller was the first one to be added as previously there were none.
 * @details If this method is defined then chatViewManager:didAddFirstChat: will not be called. The delegate must respond
 * to one of the two methods.
 */
- (void)chatViewManager:(VSeeChatViewManager *)manager didAddChat:(VSeeChatController *)chatController isFirst:(BOOL)first;

/** A chat view controller was added to the recent chat controllers
 * @param manager the chat view manager
 * @param first YES if the chat controller was the first one to be added as previously there were none.
 * @details This will only be called if chatViewManager:didAddChat:isFirst: is not defined.
 */
- (void)chatViewManager:(VSeeChatViewManager *)manager didAddFirstChat:(BOOL)first __attribute__((deprecated("Use chatViewManager:didAddChat:isFirst: instead.")));

/** A chat view controller was removed
 * @param manager The chat view manager.
 * @param chatController The chat controller for the chat that was removed.
 * @param last YES if the removed chat controller was the last one or NO if there are others left.
 * @details If this method is defined then chatViewManager:didRemoveLastChat: will not be called. The delegate must respond
 * to one of the two methods.
 */
- (void)chatViewManager:(VSeeChatViewManager *)manager didRemoveChat:(VSeeChatController *)chatController wasLast:(BOOL)last;

/** A chat view controller was removed
 * @param manager The chat view manager.
 * @param last YES if the removed chat controller was the last one or NO if there are others left.
 * @details This will only be called if chatViewManager:didRemoveChat:wasLast: is not defined.
 */
- (void)chatViewManager:(VSeeChatViewManager *)manager didRemoveLastChat:(BOOL)last __attribute__((deprecated("Use chatViewManager:didRemoveChat:wasLast: instead.")));

/** Called after a chat controller's view is loaded into memory
 * @param manager The chat view manager.
 * @param chatController The chat controller whose view as loaded.
 * @details A chat controller's view hierarchy may be loaded lazily only when it is needed for display.
 * This method is usually used to perform any additional initialization on views such as the `leftToolbar`.
 */
- (void)chatViewManager:(VSeeChatViewManager *)manager didLoadChat:(VSeeChatController *)chatController;

/** Number of unread chats for the application badge number has changed
 * @param manager the chat view manager
 * @param unreadChats the number of unread chat messages to display as the badge of the app icon in the Springboard
 * @return YES if the chat view manager should set the application icon badge number directly or NO if the delegate
 * will handle setting the application icon badge number.
 * @details If this method is not implemented the chat view manager will set the application icon badge number
 * itself as if YES was returned. This method is useful if the app does not want to include unread VSee chats in
 * its app icon badge number or if it wants to combine them with other notifications. Logging out of VSeeKit will
 * cause the unread chat app icon badge number to be reset to 0.
 */
- (BOOL)chatViewManager:(VSeeChatViewManager *)manager setApplicationIconBadgeNumber:(NSInteger)unreadChats;

@end

NS_ASSUME_NONNULL_END
