/**
 * @file VSeeServerConnection.h
 * Class representing a connection to the VSee server
 *
 * @copyright (c) 2014 VSee Lab, Inc. All rights reserved.
 * @ingroup iOSFramework
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * @ingroup iOSFramework
 * @{
 */

@protocol VSeeServerConnectionDelegate;

/** The various login states VSeeKit can be in */
typedef NS_ENUM(NSInteger, VSeeLoginState) {
    VSeeLoginStateInitializing, ///< Initializing VSeeKit
    VSeeLoginStateConnecting,   ///< Connecting to VSee server
    VSeeLoginStateIdle,         ///< Waiting for user to enter credentials
    VSeeLoginStateTryLogin,     ///< Attempting to login with credentials
    VSeeLoginStateLoggedIn,     ///< Successfully logged in
    VSeeLoginStateTryLogout,    ///< Trying to logout
    VSeeLoginStateCannotTooOld  ///< Login is not permitted, VSeeKit is too old
};


/**
 * Singleton class representing a connection to the VSee server
 */
@interface VSeeServerConnection : NSObject

/** The current login state of the user for the VSee server */
@property (assign, readonly, nonatomic) VSeeLoginState loginState;

/** The server connection delegate */
@property (weak, nullable, nonatomic) id <VSeeServerConnectionDelegate> delegate;

/** App Name displayed in error dialogs and messages
 * @details This name is automatically set by looking in order for at the following property list keys from your
 * app's bundle:
 *  -# localized CFBundleDisplayName
 *  -# non-localized CFBundleDisplayName
 *  -# localized CFBundleName
 *  -# non-localized CFBundleName
 */
@property (copy, nonatomic) NSString *appName;

/** App Name on iOS App Store
 * @details This is used to direct the user to the App Store update page for the app if the version of VSeeKit
 * it was built with is too old. */
@property (copy, nullable, nonatomic) NSString *appStoreName;

/** App Id on iOS App Store
 * @details This is used to direct the user to the App Store update page for the app if the version of VSeeKit
 * it was built with is too old. */
@property (copy, nullable, nonatomic) NSString *appStoreId;

/** Account name
 * @details This is the last account name to successfully login even if the user is not currently logged in. */
@property (copy, readonly, nullable, nonatomic) NSString *accountName;

/** Get the XMPP server address
* @return A string containing the address of the XMPP server the application is connected to.
*/
@property (copy, readonly, nonatomic) NSString *serverAddress;

/** App group identifier
 * @details The app group is used allow multiple applications to access a shared keychain for securely storing user
 * data. The format of the app group identifier is as follows:
 *  - iOS: `group.<group name>`
 *  - macOS: `<team identifier>.<group name>` */
@property (copy, nullable, nonatomic) NSString *appGroupId;

/** Whether or not to enable auto accepting calls from remote users.
 * @details If autoAccept is YES and autoAcceptUsers is nil then calls from all remote users will be accepted.
 * This reverts to the default setting of NO whenever the user is logged out. */
@property (assign, nonatomic) BOOL autoAccept;

/** Array of account names to auto accept video calls from when autoAccept is YES
 * @details The account names in this array are evaluated as regular expressions to see if a particular
 * remote user's calls should be auto accepted. Note that all users who are auto accepted also automatically
 * bypass Contact Security. Thus this serves as both the list of users to auto accept calls from and a list
 * of users who can send unsolicited 1-1 chat messages to the local user even if they are not in the local
 * user's address book.
 * @note If autoAcceptUsers is nil and autoAccept is YES then calls and chats from all remote users will be
 * accepted. This reverts to the default of nil whenever the user is logged out. */
@property (strong, nullable, nonatomic) NSArray<NSString *> *autoAcceptUsers;

/** Array of account names that can bypass Contact Security
 * @details The account names in this array are evaluated as regular expressions to see if a particular remote
 * user should be whitelisted for Contact Security. The white listed users can bypass Contact Security to send
 * unsolicted chat messages and call the local user even if they are not in the local user's address book. */
@property (strong, nullable, nonatomic) NSArray<NSString *> *whiteListUsers;

/** Time in milliseconds to wait as receiver for call to be accepted before it is treated as missed
 * @details Default is 30,000 msec (30 seconds). Waiting in the background for more than 3 minutes will generally
 * fail due to system limits on background time. */
@property (assign, nonatomic) uint32_t waitForAcceptDuration;

/** Device token for VoIP push notifications */
@property (strong, nullable, nonatomic) NSData *voipDeviceToken __deprecated_msg("VoIP push notifications are deprecated. Use `apnsDeviceToken` instead.");

/** Device token for APNS push notifications */
@property (strong, nullable, nonatomic) NSData *apnsDeviceToken;

/** Sandbox push mode
 * @details When an app is signed with a development certificate the device token will not work over the production
 * push network. If you are building with a development certificate you must set this flag to true so that pushes
 * will go through the sandbox push network for testing. The default is false. */
@property (assign, nonatomic) BOOL sandboxPushMode;

/** Disable push
 @details If this is true, push registration will not occur and no push notifications will be received. Default is NO.
 * Push registration must be disabled before login for this option to take effect. */
@property (assign, nonatomic) BOOL disablePush;

/** Disable history
 * @details Disables chat and call history. Default is NO. History must be disabled before login for this option
 * to take effect.
 * @note Chat history is still retrievable from the the server but is not saved locally when history is disabled. */
@property (assign, nonatomic) BOOL disableHistory;

/** The default VSee server connection object */
+ (VSeeServerConnection *)defaultConnection;

/** Whether the API Key and Secret have been validated to be able to use VSeeKit */
+ (BOOL)validated;

/** Validate the API Key and Secret to be able to use VSeeKit
 * @param apiKey The API Key that uniquely identifies your organization
 * @param secretHash The SHA256 hash of the API Secret of your organization encoded as hex
 * @param completion A block object to be run when the validation completes. This block has no return value and
 * takes a Boolean argument that indicates whether the credentials were successfully validated. If validation was
 * unsuccessful the error is passed in the second parameter.
 * @details Before you can use VSeeKit you must call this method to validate your organization's credentials.
 * Your API Key and API Secret are provided by VSee when you are granted access to VSeeKit. The API secret should
 * not be stored in plain text in the application. It should be SHA256 hashed and stored as a byte sequence or as
 * a decodable type such as hex. A tool like http://hash.online-convert.com/sha256-generator can be used to
 * generate the SHA256 hash from the API secret.
 *
 * The error codes passed to the completion block indicate whether or not validation should be retried. Error codes
 * less than zero indicate that validation has permanently failed and should not be retried. The possible error
 * codes are:
 *  - -1 = Invalid credentials
 *  -  1 = Network error
 *  -  2 = Bad response from server
 *  - 1000 = Unknown error
 */
+ (void)validateKey:(NSString *)apiKey secretHash:(NSString *)secretHash completion:(void (^)(BOOL success, NSError * _Nullable error))completion;

/** Get the VSee server connection state
 * @return A bool indicating true if the app is connected to the VSee server or false if not.
 */
- (BOOL)isConnected;

/** Login to the VSee server using credentials
 * @param accountName VSee account name
 * @param password password
 * @details The login session will be configured as a Web API login session with additional options to not present
 * any UI to add to the address book or place calls from chat views. In addition update needed dialogs
 * will not be displayed when interacting with newer VSee clients that have features not supported by this version
 * of VSeeKit. This method will throw an exception if the user is already logged in or in the process of logging
 * in or out.
 */
- (void)loginUser:(NSString *)accountName password:(NSString *)password;

/** Login to the VSee server using saved credentials
 * @details By default the account name and a token allowing the user to login again are saved after a successful
 * login. This method attempts to login again using these credentials if any are present. It is safe to call this
 * even if there are no credentials saved as no action will be taken. If there are stored credentials the login state
 * will change to VSeeLoginStateTryLogin while login is attempted.
 */
- (void)loginRememberedUser;

/** Login to the VSee server using saved credentials shared in app group keychain
 * @details By default the account name and a token allowing the user to login again are saved after a successful
 * login to the app group's keychain. This method attempts to login again using these credentials if any are present.
 * It is safe to call this even if there are no credentials saved as no action will be taken. If there are stored
 * credentials the login state will change to VSeeLoginStateTryLogin while login is attempted.
 * @return A bool indicating true if the saved information is enough to attempt app login.
 */
- (BOOL)loginRememberedUserInAppGroup;

/** Logout the current user
 * @details This also deletes any persistent data associated with the user such as the saved credentials or locally stored
 * call or chat history.
 */
- (void)logout;

@end


/** Protocol to inform the server connection delegate of changes to the login state
 * @details Callbacks to the delegate will always occur on the main thread.
 */
@protocol VSeeServerConnectionDelegate <NSObject>

/** The login state has changed
 * @param connection the server connection object
 * @param loginState the new login state
 */
- (void)serverConnection:(VSeeServerConnection *)connection loginState:(VSeeLoginState)loginState;

/** A new status message is available
 * @param connection the server connection object
 * @param message a string showing the login status that is suitable for display
 */
- (void)serverConnection:(VSeeServerConnection *)connection message:(NSString *)message;

@optional

/** A connection to the VSee server has been started
 * @details This method is called whenever a new connection or reconnection is started to the VSee server.
 */
- (void)startedServerConnection:(VSeeServerConnection *)connection;

/** A connection to the VSee server has been completed
 * @param connection The server connection object.
 * @param timings A dictionary containing timing data in milliseconds.
 * details When a server connection or reconnection is completed this method will be called with the timing data.
 */
- (void)serverConnection:(VSeeServerConnection *)connection completeTimings:(NSDictionary *)timings;

@end

/** @} */

NS_ASSUME_NONNULL_END
