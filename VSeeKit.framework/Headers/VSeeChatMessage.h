/**
 * @file VSeeChatMessage.h
 * Data for the individual chat messages
 *
 * @details This object contains the base data for chat messages
 * @copyright (c) 2017 VSee Lab, Inc. All rights reserved.
 * @ingroup VSeeKitCore
 */

#import <TargetConditionals.h>
#if TARGET_OS_OSX
#import <Cocoa/Cocoa.h>
typedef NSImage VSeeChatImage;
#else
#import <UIKit/UIKit.h>
typedef UIImage VSeeChatImage;
#endif

NS_ASSUME_NONNULL_BEGIN

@protocol VSeeChatMessageDelegate, VSeeChatMessageMediaDataSource, VSeeChatMessageMediaDelegate, VSeePhotoMediaProtocol, VSeeMediaProtocol;

/** @enum VSeeIMSendStatus
 * The types of sending statuses for outgoing chat messages
 */
typedef NS_ENUM(NSInteger, VSeeChatSendStatus) {
    VSeeChatSendStatusInit,         ///< No status, initializing message
    VSeeChatSendStatusSending,      ///< Message is being sent to XMPP server
    VSeeChatSendStatusSent,         ///< Message has been delivered to the XMPP server
    VSeeChatSendStatusSeen,         ///< Message has been seen by the remote user
    VSeeChatSendStatusAcknowledged, ///< Priority Message has been acknowledged by the remote user
    VSeeChatSendStatusOffline,      ///< Remote user is offline so XMPP server has queued it for later delivery
    VSeeChatSendStatusError         ///< Message could not be delivered to the XMPP server
};

/** The possible Chat States according to XEP-0085. */
typedef NS_ENUM(NSInteger, VSeeChatState) {
    VSeeChatStateActive       =  1,     /**< User is actively participating in the chat session. */
    VSeeChatStateComposing    =  2,     /**< User is composing a message. */
    VSeeChatStatePaused       =  4,     /**< User had been composing but now has stopped. */
    VSeeChatStateInactive     =  8,     /**< User has not been actively participating in the chat session. */
    VSeeChatStateGone         = 16,     /**< User has effectively ended their participation in the chat session. */
    VSeeChatStateInvalid      = 32      /**< Invalid type. */
};

/** @enum VSeeChatMessageType
 *  These are the types of chat messages
 */
typedef NS_ENUM(NSInteger, VSeeChatMessageType) {
    VSeeChatMessageTypeUnknown,         ///< Chat message of an unknown or uninitialized type
    VSeeChatMessageTypeNormal,          ///< Normal chat message, either text or image
    VSeeChatMessageTypeSystem,          ///< Generic system message sent from one client to another
    VSeeChatMessageTypeNewUser,         ///< New user invited by the current user has come online
    VSeeChatMessageTypeUpdateNeeded,    ///< Message from one client telling older client to update to use newer features
    VSeeChatMessageTypeCarbonCopy       ///< Carbon copied message sent by another system signed in on the same account
};

/** Possible chat marker types sent down by server
 */
typedef NS_ENUM(NSInteger, VSeeChatMarkerType) {
    VSeeChatMarkerTypeSeen,                 ///< Marker indicates message has been seen
    VSeeChatMarkerTypeAcknowledged          ///< Marker indicates priority message has been acknowledged
};

/** Class to contain chat marker data for received "displayed" or "acknowledged" markers
 */
@interface VSeeChatMarker : NSObject
@property (strong, nonatomic) NSString *vseeId;         ///< Sender of the chat marker
@property (strong, nonatomic) NSString *messageId;      ///< Message id of the original marked message
@property (strong, nonatomic) NSDate *date;             ///< Date and time the chat marker was received
@property (assign, nonatomic) VSeeChatMarkerType type;  ///< Chat marker type, "displayed" or "acknowledged"
@property (assign, nonatomic) BOOL isFromMe;            ///< Chat marker originates from me from another device
@property (assign, nonatomic) BOOL isWaitingRoom;       ///< Chat marker for waiting room message
@end

/** Chat marker for a group chat room */
@interface VSeeGroupChatMarker : VSeeChatMarker
@property (copy, nonnull, nonatomic) NSString *roomId;
@end

/** Chat marker for an in call chat */
@interface VSeeInCallChatMarker: VSeeChatMarker
@end

typedef void (^VSeeChatMessageMetaDataCallback)(NSURL *_Nullable url,
                                                NSString *_Nullable title,
                                                VSeeChatImage *_Nullable contentImage,
                                                NSError *_Nullable error);

/** Data for the individual chat messages
 *
 * @details This object contains the data from an individual chat message.
 * @ingroup VSeeKitCore
 */
@interface VSeeChatMessage : NSObject

/** Delegate object to handle chat message sending status */
@property (weak, nullable, nonatomic) id<VSeeChatMessageDelegate> delegate;
/** Delegate object for sending the chat message's media data */
@property (weak, nullable, nonatomic) id<VSeeChatMessageMediaDataSource> mediaDataSource;
/** Delegate object for the upload or download progress of the chat message's media data*/
@property (weak, nullable, nonatomic) id<VSeeChatMessageMediaDelegate> mediaDelegate;

@property (assign, nonatomic) VSeeChatMessageType type;         ///< Message type
@property (assign, nonatomic) VSeeChatSendStatus sendStatus;    ///< Send status of the message
@property (copy, nonnull, nonatomic) NSString *senderId;        ///< Account name of the sender
@property (copy, nonnull, nonatomic) NSString *senderServer;    ///< Server of the sender
@property (copy, nonnull, nonatomic) NSString *recipientId;     ///< Account name of the recipient if only one
@property (copy, nonnull, nonatomic) NSString *recipientServer; ///< Server of the recipient if only one
@property (copy, nonnull, nonatomic) NSString *displayName;     ///< Name to display for the sender
@property (copy, nonnull, nonatomic) NSString *message;         ///< Text of the message
@property (copy, nonnull, nonatomic) NSString *messageId;       ///< Unique message id
@property (copy, nonnull, nonatomic) NSString *subject;         ///< Subject of the message
@property (copy, nonnull, nonatomic) NSString *thread;          ///< Thread the message belongs to
@property (strong, nonnull, nonatomic) NSDate *date;    ///< Time and date this message was sent
@property (readonly, nonatomic) BOOL incoming;          ///< Whether this message is an incoming message
@property (readonly, nonatomic) BOOL isMediaMessage;    ///< Whether this message contains image or other media content
@property (readonly, nonatomic) BOOL isFileMessage;
@property (assign, nonatomic) BOOL fromArchive;         ///< Whether this message was loaded from the archive rather than being received directly
@property (assign, nonatomic) BOOL read;                ///< Whether this message has been read already, particularly when loading from the archive
@property (strong, nullable, readonly, nonatomic) id<VSeePhotoMediaProtocol> photoMedia;    ///< Object that represents an image attachment
@property (strong, nullable, readonly, nonatomic) id<VSeeMediaProtocol> fileMedia;    ///< Object that represents a file attachment

// Fetched metadata for URL in message
@property (strong, nullable, readonly, nonatomic) NSURL *fetchedMetadataURL API_AVAILABLE(ios(13.0), macos(10.15)); ///< Result of `fetchUrlMetadataFromMessageWithCompletion`
@property (strong, nullable, readonly, nonatomic) NSString *urlMetadataTitle API_AVAILABLE(ios(13.0), macos(10.15)); ///< Result of `fetchUrlMetadataFromMessageWithCompletion`
@property (strong, nullable, readonly, nonatomic) VSeeChatImage *urlContentImage API_AVAILABLE(ios(13.0), macos(10.15)); ///< Result of `fetchUrlMetadataFromMessageWithCompletion`
@property (strong, nullable, readonly, nonatomic) VSeeChatImage *urlContentIcon API_AVAILABLE(ios(13.0), macos(10.15)); ///< Result of `fetchUrlMetadataFromMessageWithCompletion`

// Priority message data
@property (assign, getter=isPriority, nonatomic) BOOL priority; ///< Is this a priority message?
@property (assign, nonatomic) NSInteger alertInterval;          ///< Priority alert interval in seconds
@property (assign, nonatomic) NSTimeInterval expireTime;        ///< Priority alert expiration time in seconds since epoch
@property (readonly, nonatomic) BOOL requireAcknowledgement;    ///< Does this priority message still require acknowledgement?

/** Return a new empty normal chat message
 * @return An empty chat message of type VSeeChatMessageTypeNormal. */
+ (nullable instancetype)chatMessage;

/** Return an empty chat message
 * @param messageType The type of chat message to create.
 * @return An empty chat message of specified type. */
+ (nullable instancetype)chatMessageWithType:(VSeeChatMessageType)messageType;

/** Indicates whether a chat message is equal to the receiver
 * @param chatMessage The chat message with which to compare the receiver.
 * @return YES if chatMessage is equivalent to the receiver, otherwise NO. */
- (BOOL)isEqualToChatMessage:(nullable VSeeChatMessage *)chatMessage;

/** Create a photo media object for this message with a new image
 * @param image The image to send with the chat message.
 * @details The photoMedia property is populated with the new photo media object. */
- (void)createPhotoMediaWithImage:(nonnull VSeeChatImage *)image;

/** Create a photo media object for this message from a URL
 * @param imageUrl The base URL the image can be downloaded from.
 * @param imageAlt The alternative text to display if unable to display the image.
 * @details The photoMedia property is populated with the new photo media object. */
- (void)createPhotoMediaWithUrl:(nonnull NSString *)imageUrl alternativeText:(nonnull NSString *)imageAlt;

/** Create a file media object for this message from a local file
 * @param fileUrl The path of the file to be uploaded.
 * @details The fileMedia property is populated with the new file media object. */
- (void)createLocalFileMediaWithUrl:(nonnull NSString *)fileUrl;

/** Create a file media object for this message from a URL
 * @param fileUrl The base URL the file can be downloaded from.
 * @param fileName The name of the file.
 * @param fileType The type of the file.
 * @param fileSize Size of the file in bytes
 * @details The fileMedia property is populated with the new file media object. */
- (void)createFileMediaWithFileURL:(NSString *)fileUrl fileName:(NSString *)fileName fileType:(NSString *)fileType fileSize:(uint32_t)fileSize;

/** Start uploading media data for the message
 * @details The media delegate will be notified when the upload completes or fails. A chat message
 * should not be sent until any media associated with it has completed uploading. */
- (void)uploadMedia;

/** Start downloading the full media data for the message
 * @details The media delegate will be notified when the download completes or fails. When a media chat
 * message is received a thumbnail image for it is queued to be downloaded automatically. Typically the
 * full media data is only downloaded lazily using this method. */
- (void)downloadMedia;

/** Send the chat message */
- (void)sendMessage;

/// If there are URL in the message, async fetch metadata from the URL
/// Only fetch metadata for the first url in the message
/// @param shouldScale Downsampling the contentImage to improve performance, not supported on MacOS
/// @param completion Completion handler for metadata, should be used for calculate size or reload
- (void)fetchUrlMetadataFromMessageShouldDoScale:(BOOL)shouldScale withCompletion:(VSeeChatMessageMetaDataCallback _Nullable)completion API_AVAILABLE(ios(13.0), macos(10.15));
@end


/** Chat message for In Call meeting chat */
@interface VSeeInCallChatMessage : VSeeChatMessage
@end

/** Chat message for a group chat room */
@interface VSeeGroupChatMessage : VSeeChatMessage
@property (copy, nonnull, nonatomic) NSString *roomId;  ///< The room id of the group chat room
@end

/** Chat message for 1-1 chat between users */
@interface VSeeOneToOneChatMessage : VSeeChatMessage
@end

/** Special chat messages that are used by VSee Clinic to notify providers of users entering their waiting rooms */
@interface VSeeWaitingRoomChatMessage : VSeeOneToOneChatMessage
/** A custom call id for the remote to use when calling back to the sender */
@property (copy, nullable, nonatomic) NSString *customCallId;
/** The expire time of the custom call id in units of seconds after epoch. Default of 0 means it does not expire. */
@property (assign, nonatomic) uint64_t callIdExpireTime;
@end


/** Protocol to inform the delegate of a chat messages sending status */
@protocol VSeeChatMessageDelegate <NSObject>
/** The chat message delivery to the server has timed out.
 * @param chatMessage The chat message that was not successfully delivered. */
- (void)chatMessageDeliveryTimedOut:(nonnull VSeeChatMessage *)chatMessage;

/** The chat message will begin sending to the server.
 * @param chatMessage The chat message that will begin sending. */
- (void)chatMessageWillBeginSending:(nonnull VSeeChatMessage *)chatMessage;

/** The chat message did finish sending to the server.
 * @param chatMessage The chat message that has finished sending.
 * @note This does not mean the message has been delivered to the server. */
- (void)chatMessageDidFinishSending:(nonnull VSeeChatMessage *)chatMessage;
@end


/** Protocol for sending a chat message's media data */
@protocol VSeeChatMessageMediaDataSource <NSObject>
/** The array of account names who should be allowed to download the media associated with a chat message
 * @param chatMessage The chat messages whose media data is affected.
 * @details The sender is always allowed to download the media. Typically this array will only contain the
 * recipient for 1-1 chat messages or the members of the group chat room for group chat messages.
 */
- (nonnull NSArray *)usersForUploadingChatMessageMedia:(nonnull VSeeChatMessage *)chatMessage;
@end


/** Protocol for the upload or download progress of a chat message's media data */
@protocol VSeeChatMessageMediaDelegate <NSObject>
/** Media data associated with a chat message was loaded from memory or local disk cache
 * @param chatMessage The chat message whose media data was loaded.
 */
- (void)chatMessageMediaDidFinishLoading:(nonnull VSeeChatMessage *)chatMessage;

/** Media data associated with a chat message has been uploaded to the media server
 * @param chatMessage The chat message whose media data was uploaded.
 * @param error An NSError object that describes the error if an error occurred.
 */
- (void)chatMessageMediaDidFinishUploading:(nonnull VSeeChatMessage *)chatMessage error:(nullable NSError *)error;

/** Media data associated with a chat message has been downloaded from the media server
 * @param chatMessage The chat message whose media data was downloaded.
 * @param error An NSError object that describes the error if an error occurred.
 */
- (void)chatMessageMediaDidFinishDownloading:(nonnull VSeeChatMessage *)chatMessage error:(nullable NSError *)error;
@end

NS_ASSUME_NONNULL_END
