/**
 * @file VSeeStatusHelper.h
 * Helper class to deal with user presence status details
 *
 * @copyright (c) 2019 VSee Lab, Inc. All rights reserved.
 * @ingroup iOSFramework
 */

#import <UIKit/UIKit.h>
#import "VSeeContact.h"

/** The sizes of status icons */
typedef NS_ENUM(NSUInteger, VSeeStatusIconSize) {
    VSeeStatusIconSizeNormal,   ///< A normal sized status icon of 22x22 points
    VSeeStatusIconSizeLarge     ///< A large sized status icon of 28x28 points
};

/**
 * Singleton class to deal with user presence status
 * @ingroup iOSUI
 */
@interface VSeeStatusHelper : NSObject

/** Return the shared status helper
 * @return the status helper object
 */
+ (VSeeStatusHelper *)sharedHelper;

/** The local user's presence status */
@property (assign, nonatomic) VSeePresenceStatus userStatus;

/** Name of the status state
 * @param status The presence status
 * @param isLocal Is the presence status for the local user or a remote user?
 * @return A string describing the status
 * @details For remote users the user settable options are obfuscated so if the user has set their status to appear busy, invisible,
 * etc. the name of the simulated state will be returned instead. For local users the true state name is returned.
 */
+ (NSString *)nameOfStatus:(VSeePresenceStatus)status local:(BOOL)isLocal;

/** Icon for a presence status
 * @param status The presence status
 * @param size The size of the icon to return
 * @return An image containing the presence status icon of the size requested
 */
+ (UIImage *)iconForStatus:(VSeePresenceStatus)status size:(VSeeStatusIconSize)size;

/** Local user status may have changed and should be updated
 * @note This is typically only used internally by VSeeKit. */
- (void)updateUserStatus;

/** Name of the status state of the local user
 * @return A string describing the local user's presence status
 * @details If the local user has set their status to appear busy, invisible, etc. the true state will be returned.
 */
+ (NSString *)nameOfLocalStatus;

/** Icon for the local user's presence status
 * @return An image containing the presence status icon of the size requested
 */
+ (UIImage *)iconForLocalStatusSize:(VSeeStatusIconSize)size;

@end
