/**
 * @file VSeeWaitingRoom.h
 * Class to manage the connection to eVisit-based Waiting Rooms
 *
 * @copyright (c) 2014 VSee Lab, Inc. All rights reserved.
 * @ingroup iOSFramework
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * @ingroup iOSFramework
 * @{
 */

@protocol VSeeWaitingRoomDelegate;

/** The types of visits that can be requested */
typedef NS_ENUM(NSInteger, VSeeWaitingRoomType) {
    VSeeWaitingRoomChat,    ///< VSee text chat
    VSeeWaitingRoomVideo,   ///< VSee video call
    VSeeWaitingRoomPhone,   ///< Phone call
    VSeeWaitingRoomAny      ///< Any visit type
};

/** Possible values for waiting room status */
typedef NS_ENUM(NSInteger, VSeeWaitingRoomStatus) {
    VSeeWaitingRoomInitialized, ///< Initialized VSeeWaitingRoom
    VSeeWaitingRoomSigningIn,   ///< In the process of signing into the waiting room and VSee server
    VSeeWaitingRoomSignedIn,    ///< Connected to the waiting room and logged in to the VSee server
    VSeeWaitingRoomRequestingVisit, ///< In the process of requesting a visit
    VSeeWaitingRoomInRoom,      ///< Patient is waiting for the visit, and shown on the provider side
    VSeeWaitingRoomRemoved,     ///< Patient was removed from the waiting room
    VSeeWaitingRoomSigningOut   ///< In the process of signing out and closing visit
};

/** Class to manage the connection to eVisit-based Waiting Rooms
 * @details Requests are made asynchronously and the delegate receives call backs when they are completed.
 * The user must first sign in using signInFirstName:lastName:userID: before a visit request can be made.
 */
@interface VSeeWaitingRoom : NSObject

@property (weak, nullable, nonatomic) id <VSeeWaitingRoomDelegate> delegate;    ///< the waiting room delegate
@property (strong, readonly, nonatomic) NSURL *endpointURL;         ///< URL of the API Endpoint for the waiting room
@property (copy, readonly, nonatomic) NSString *roomID;             ///< Code for the specific room at the API endpoint
@property (assign, readonly, nonatomic) VSeeWaitingRoomStatus status;   ///< Waiting room status

/** Initializes a newly created waiting room
 * @param roomID the short room code indicating a specific room of the API endpoint
 * @param endpointURL the URL of the API end point for the waiting room
 * @return the waiting room object
 */
- (instancetype)initWithRoom:(NSString *)roomID atEndPoint:(NSURL *)endpointURL;

- (instancetype)init __attribute__((unavailable("Invoke the designated initializer initWithRoom:atEndPoint: instead.")));
+ (instancetype)new __attribute__((unavailable("Invoke the designated initializer initWithRoom:atEndPoint: instead.")));

/** Lookup the information for the waiting room */
- (void)lookupRoomInfo;

/** Connect to the waiting room and sign in
 * @param firstName the user's first name, must not be nil with at least one non-whitespace character (required)
 * @param lastName the user's last name, can be nil (optional)
 * @param userID a code that uniquely identifies the user, can be nil and must not be more than 36 characters (optional)
 * @details This signs the user into the waiting room and into the VSee server using the default connection of VSeeServerConnection.
 * This is a convenience method that calls signInFirstName:lastName:userID:token: with a nil token.
 */
- (void)signInFirstName:(NSString *)firstName lastName:(nullable NSString *)lastName userID:(nullable NSString *)userID;

/** Connect to the waiting room and sign in using the app token
 * @param firstName the user's first name, must not be nil with at least one non-whitespace character (required)
 * @param lastName the user's last name, can be nil (optional)
 * @param userID a code that uniquely identifies the user, can be nil and must not be more than 36 characters (optional)
 * @param token a code that used for the API server to authorize the app, can be nil if not required for this API Endpoint (optional)
 * @details This signs the user into the waiting room and into the VSee server using the default connection of VSeeServerConnection.
 */
- (void)signInFirstName:(NSString *)firstName lastName:(nullable NSString *)lastName userID:(nullable NSString *)userID token:(nullable NSString *)token;

/** Connect to the waiting room and sign in using the app token
 * @param firstName the user's first name, must not be nil with at least one non-whitespace character (required)
 * @param lastName the user's last name, can be nil (optional)
 * @param userID a code that uniquely identifies the user, can be nil and must not be more than 36 characters (optional)
 * @param token a code that used for the API server to authorize the app, can be nil if not required for this API Endpoint (optional)
 * @param extras a dictionary specifying additional key-value pairs to send to the API server, can be nil (optional)
 * @details This signs the user into the waiting room and into the VSee server using the default connection of VSeeServerConnection.
 */
- (void)signInFirstName:(NSString *)firstName lastName:(nullable NSString *)lastName userID:(nullable NSString *)userID token:(nullable NSString *)token extraFields:(nullable NSDictionary *)extras;

/** Initiate a visit request
 * @param visitType the type of visit requested
 * @param reason the reason for the visit, can be nil (optional)
 * @details The user must have signed into the waiting room before a visit request can be made.
 */
- (void)requestVisitType:(VSeeWaitingRoomType)visitType reason:(nullable NSString *)reason;

/** Initiate a visit request
 * @param dataDict the dictionary for the customized data (required)
 * @details The user must have signed into the waiting room before a visit request can be made.
 */
- (void)requestVisitWithDictionary:(NSDictionary *)dataDict;

/** Close the visit and sign out from all waiting room services */
- (void)signOut;

/** Check if any providers are available in the waiting room
 * @details This can be called anytime once the user has signed into the waiting room.
 */
- (void)checkProvidersAvailable;

@end


/** Protocol to inform the waiting room delegate of the status of waiting room requests
 * @details Callbacks to the delegate will always occur on the main thread.
 */
@protocol VSeeWaitingRoomDelegate <NSObject>

/** The lookup for room information has been completed
 * @param room the waiting room object
 * @param info a dictionary of the information for the waiting room. If the room exists, it will contain the "Room", "Providers" and "Account" fields.
 */
- (void)waitingRoom:(VSeeWaitingRoom *)room roomInfo:(NSDictionary *)info;

/** The room status has changed
 * @param room the waiting room object
 * @param status the new room status
 * @details If the room status is VSeeWaitingRoomInitialized, VSeeWaitingRoomSignedIn, or VSeeWaitingRoomInRoom
 * the previous request is complete.
 */
- (void)waitingRoom:(VSeeWaitingRoom *)room status:(VSeeWaitingRoomStatus)status;

/** The check for provider availability has completed
 * @param room the waiting room object
 * @param providers an array of the names of all providers currently available in the waiting room
 */
- (void)waitingRoom:(VSeeWaitingRoom *)room providersAvailable:(NSArray *)providers;

/** An error occured with the current waiting room request
 * @param room the waiting room object
 * @param error the error object
 */
- (void)waitingRoom:(VSeeWaitingRoom *)room error:(NSError *)error;

@end

/** @} */

NS_ASSUME_NONNULL_END
