/**
 * @file VSeeWindow.h
 * Base VSee window class
 *
 * @details The top level window of any app using the VSee iOS Framework must be an instance
 * of VSeeWindow or one of its subclasses.
 * @copyright (c) 2013 VSee Lab, Inc. All rights reserved.
 * @ingroup iOSFramework
 */

#import <UIKit/UIKit.h>

/**
 * Base VSee window class
 *
 * @details The top level window of any app using the VSee iOS Framework must be an instance
 * of VSeeWindow or one of its subclasses.
 * @ingroup iOSFramework
 */
@interface VSeeWindow : UIWindow

@property (assign, nonatomic) uint64_t lastTouchMilliCount;     ///< The last time in milliseconds that this window received a touch event

+ (uint64_t)lastTouchMilliCount;                                ///< The last time in milliseconds that any VSeeWindow received a touch event

@end
