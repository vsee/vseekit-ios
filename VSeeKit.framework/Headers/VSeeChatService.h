/**
 * @file VSeeChatService.h
 * Singleton that provides low level chat features
 *
 * @copyright (c) 2017 VSee Lab, Inc. All rights reserved.
 * @ingroup VSeeKitCore
 */

#import <Foundation/Foundation.h>
#import "VSeeChatMessage.h"

NS_ASSUME_NONNULL_BEGIN

/** The possible group chat affliations from XEP-0045. */
typedef NS_ENUM(NSInteger, VSeeGroupAffiliationType) {
    VSeeGroupAffiliationTypeUnknown,       /**< User affiliation is not yet known */
    VSeeGroupAffiliationTypeNonMember,     /**< User is not a member of the group chat. (No affiliation or an outcast) */
    VSeeGroupAffiliationTypeMember,        /**< User is a member of the group chat. */
    VSeeGroupAffiliationTypeAdmin,         /**< User is an admin for the group chat. */
    VSeeGroupAffiliationTypeOwner          /**< User is an owner of the group chat. */
};

@interface VSeeGroupAffiliation: NSObject
@property (strong, nonnull, nonatomic) NSString *username;
@property (strong, nonnull, nonatomic) NSString *server;
@property (assign, nonatomic) VSeeGroupAffiliationType affliation;
@property (assign, nonatomic) BOOL subscription;
@end


typedef NS_ENUM(NSInteger, VSeeMeetingAffiliationType) {
    VSeeMeetingAffiliationTypeNonMember,     /**< User is not a member of the meeting chat. (No affiliation or an outcast) */
    VSeeMeetingAffiliationTypeMember,        /**< User is a member of the meeting chat. */
    VSeeMeetingAffiliationTypeAdmin,         /**< User is an admin for the meeting chat. */
    VSeeMeetingAffiliationTypeOwner          /**< User is an owner of the meeting chat. */
};


typedef NS_ENUM(NSInteger, VSeeMeetingRoleType) {
    VSeeMeetingRoleTypeOther,         /**< User role is none or invalid in the meeting chat. */
    VSeeMeetingRoleTypeMember,        /**< User role is visitor or participants of the meeting chat. */
    VSeeMeetingRoleTypeModerator      /**< User is a moderator of the meeting chat. */
};

typedef NS_ENUM(NSInteger, VSeeGroupOccupantChangeType) {
    VSeeGroupOccupantChangeAddExisting,  /**< User should be added to UI but was already in group chat room */
    VSeeGroupOccupantChangeAddNew,       /**< User was just added to group chat room and needs a join message added */
    VSeeGroupOccupantChangeRemove,       /**< User left group chat room */
    VSeeGroupOccupantChangeUpdate,       /**< Existing user was updated (affiliation, role, or subscribed sate */
    VSeeGroupOccupantNotChanged
};

@protocol VSeeChatReceiveDelegate;

@interface VSeeChatService : NSObject

+ (instancetype)sharedService;

/** The delegate to handle incoming chat messages
 * @note By default this is the VSeeChatViewManager.
 */
@property (weak, nullable, nonatomic) id<VSeeChatReceiveDelegate> delegate;

/** Syncing chat
 * @details This is true whenever any chat synchronizing is being done with the XMPP server. */
@property (assign, nonatomic) BOOL syncingChat;

/** Disable chat sync
 * @details When chat sync is disabled no chat history will be retrieved from the server and group chat rooms will
 * not be joined on login. Default is NO. Chat sync must be disabled before login for this option to take effect. */
@property (assign, nonatomic) BOOL disableChatSync;

/** Generate a new UUID
 * @return The new UUID.
 * @details This method is most commonly used to generate a unique id value for a message id. */
+ (NSString *)generateUUID;
- (NSString *)generateUUID;

/** Send the local users XEP-0085 chat state to another user
 * @param accountName The account name of the user to send the chat state to
 * @param chatState The XEP-0085 chat state to send */
- (void)sendChatState:(VSeeChatState)chatState toUser:(NSString *)accountName;

/** Generate a unique name for a new group chat room
 * @return A name to use in creating a new group chat room */
- (NSString *)generateGroupChatRoomName;

/** Create or join a group chat room
 * @param displayName The display name of the group chat room
 * @details If a group chat room of the specified name already exists on the server, the local user
 * will join it. Otherwise a new group chat room is created.
 * Will return room name/chat id */
- (NSString *)startGroupChatRoom:(NSString *)displayName;

/** Join existing group chat room
 * @param roomName The name of the group chat room */
- (void)joinGroupChatRoom:(NSString *)roomName;

/** Leave a group chat room
 * @param roomName The name of the group chat room */
- (void)leaveGroupChatRoom:(NSString *)roomName;

/** Delete a group chat room
 * @param roomName The name of the group chat room
 * @details Owner can delete group chat. */
- (void)deleteGroupChatRoom:(NSString *)roomName;

/** Invite a user to join a group chat room
 * @param accountName The account name of the user to invite
 * @param roomName The name of the group chat room
 */
- (void)inviteUser:(NSString *)accountName toGroupChatRoom:(NSString *)roomName;

/** Set group chat room info
 * @param roomName The name of the group chat room.
 * @param displayName The name to present in the UI as the group chat room name.
 * @param persistent Whether the room will persist on the server even if it contains no occupants.
 * @details By default rooms have no display name and are not persistent. When an occupant of the room changes the
 * display name or persistent status all of the room occupants are notified.
 */
- (void)setGroupChatRoom:(NSString *)roomName displayName:(nullable NSString *)displayName persistent:(BOOL)persistent;

/** Request an update of a group chat room's occupants
 * @param roomName The group chat room name.
 * @details The occupant lists for all group chat rooms are automatically updated when logging in
 * or reconnecting to the server after the app returns to the foregound. This method can be used to
 * request an update at another time, such as after inviting users a group chat.
 */
- (void)requestOccupantsforRoom:(NSString *)roomName;

/** Set affiliation for an user in a group
 * @param roomName The group chat room name.
 * @param username The account name of the user to send the chat state to
 * @param affiliationType The affiliation to change
 */
- (void)setGroupAffiliation:(NSString *)roomName username:(NSString *)username affiliation:(VSeeGroupAffiliationType)affiliationType;

/** Remove an user from a group
 * @param username The account name of the user to remove from the group
 * @param roomName The group chat room name.
 */
- (void)remove:(NSString *)username fromRoom:(NSString *)roomName;

@end


/** Protocol for the object that handles incoming chat messages
 */
@protocol VSeeChatReceiveDelegate <NSObject>

/** Receive a chat message
 * @param message The new chat message.
 * @note Chat messages are received through various pathways including directly from the server,
 * delayed delivery while offline, or loaded from the chat archive. Thus, received messages may
 * be duplicated or delivered out of order. The chat message id should always be used to check for
 * uniqueness.
 */
- (void)receiveMessage:(VSeeChatMessage *)message;

/** Receive a chat marker
 * @param chatMarker The new chat marker.
 * @details Per the XEP-0333 spec, chat markers must only move forward. Less significant chat markers are
 * only sent if they are later than a more significant marker. Thus a "displayed" marker implies that all earlier
 * messages have been displayed whether or not they are individually marked.
 */
- (void)receiveChatMarker:(VSeeChatMarker *)chatMarker;

/** Receive a chat state update
 * @param state The new XEP-0085 chat state.
 * @accountName The remote user whose chat state has changed.
 */
- (void)receiveChatState:(VSeeChatState)state forUser:(NSString *)accountName;

/** A one-to-one chat message was not able to be immediately delivered because the user is offline
 * @param accountName The account name of the remote user who is offline.
 * @param messageId The message id of the message that the remote user was offline for.
 * @details The message will be stored on the server for delayed delivery when the user logs in again.
 */
- (void)messageEventUserOffline:(NSString *)accountName forMessage:(NSString *)messageId;

/** A one-to-one chat message was delivered to the XMPP server
 * @param accountName The account name of the remote user who the message is intended for.
 * @param messageId The message id of the delivered message.
 */
- (void)messageEventDelivered:(NSString *)accountName forMessage:(NSString *)messageId;

/** A new participant has joined the In Call meeting chat
 * @param accountName The account name of the user who joined the meeting
 */
- (void)userDidJoinMeetingChat:(NSString *)accountName;

/** A participant has left the In Call meeting chat
 * @param accountName The account name of the user who left the meeting
 */
- (void)userDidLeaveMeetingChat:(NSString *)accountName;

- (void)meetingAffiliationRoleChangeForUser:(NSString *)accountName newRole:(VSeeMeetingRoleType)roleType newAffiliation:(VSeeMeetingAffiliationType)affiliationType;

/** Updates the group chat UI to reflect the membership status of a user
 * @param accountName The account name of the user who has joined or left the chat room.
 * @param affiliation The user's membership status in the room.
 * @param available True if the user has joined the room or false if the user has left the room.
 * @param roomName The name of the group chat room.
 * @param subscribed The account is subscribed to the room.
 * @param isOnlyInvited The account is invited to the room but has not accepted or declined yet.
 * @details If the local user joins a group chat room either by creating it or being invited by
 * another user, then this method will be called with the local user's account name.
 */
- (void)updateGroupChatOccupant:(NSString *)accountName affiliation:(VSeeGroupAffiliationType)affiliation available:(BOOL)available inRoom:(NSString *)roomName subscribed:(BOOL)subscribed isOnlyInvited:(BOOL)isOnlyInvited changeType:(VSeeGroupOccupantChangeType)changeType;

/** Notification that occupant list is complete
 * @param roomName The name of the group chat room.
 * @details Once the affiliation list has finished loading this is called.
 */
- (void)groupChatRoomComplete:(NSString *)roomName;

/** Updates the group chat UI to reflect a change in group chat room info
 * @param roomName The name of the group chat room.
 * @param displayName The name to present in the UI as the group chat room name.
 * @param persistent Whether the room will persist on the server even if it contains no occupants.
 * @details By default rooms have no display name and are not persistent. When an occupant of the room changes the
 * display name or persistent status all of the room occupants are notified.
 */
- (void)updateGroupChatRoom:(NSString *)roomName displayName:(nullable NSString *)displayName persistent:(BOOL)persistent;

/** Refresh messages in a group chat room
 * @param roomName The name of the group chat room
 * @param shouldSync Boolean to indicate if recent messages should be synced
 * @details After reconnecting to the XMPP server, this method is called to indicate that the server should be queried for
 * any group chat messages that were missed while disconnected. */
- (void)refreshGroupChatRoom:(NSString *)roomName shouldSyncMessage:(BOOL)shouldSync;

/// Refresh messages in a collections of group chat rooms
/// @param roomNames Dictionary contains key = room name, value = Boolean to indicate if recent messages should be synced
/// @param isInitialList Indicate if the roomNames is subsequent room adds or initial list
- (void)refreshGroupChatRooms:(nonnull NSDictionary<NSString *, NSNumber *>*)roomNames isInitialList:(BOOL)isInitialList;

/** Reset the chat UI after logout */
- (void)reset;

/// Clear the messages for the chatId before adding new messages
/// @param chatId id of the chat to clear UI
/// @param timeStamp all messages before this timeStamp should be cleared, timeStamp is in milliseconds
- (void)clearMessagesForChatId:(NSString *)chatId before:(uint64_t)timeStamp;

@optional

/** Whether one to one chat is being synced with the server
 * @details This can be used by the UI so unnecessary activity can be avoided when it is likely blocks of messages will be loaded.
 */
@property (assign, nonatomic) BOOL syncingOneToOneChat;

/** Whether group chat is beng synced with the server
 * @details After reconnecting to the XMPP server, group chat is automatically synced with the server. */
@property (assign, nonatomic) BOOL syncingGroupChat;

@end

NS_ASSUME_NONNULL_END
