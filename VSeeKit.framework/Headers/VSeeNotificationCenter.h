/**
 * @file VSeeNotificationCenter.h
 * Class to manage queuing and displaying call and chat notifications while app is in the foreground
 *
 * @copyright (c) 2012 VSee Lab, Inc. All rights reserved.
 * @ingroup iOSFramework
 */

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

NS_ASSUME_NONNULL_BEGIN

/** @enum VSeeNotificationType
 * These are various types of incoming message notifications
 */
typedef NS_ENUM(NSInteger, VSeeNotificationType) {
    VSeeNotificationOneToOneChat,       ///< An incoming 1-1 chat has arrived.
    VSeeNotificationInCallChat,         ///< An incoming in call chat has arrived.
    VseeNotificationGroupChat,          ///< An incoming group chat has arrived.
    VSeeNotificationCall,               ///< An incoming video call is ringing.
    VSeeNotificationWaitingRoomChat     ///< An incoming waiting room chat has arrived. (VSee client only)
};

/** @enum VSeeDialingStatus
 * These are the possible statuses of outgoing video calls
 *
 * @details Outgoing video calls are placed to every end point that the remote user is logged into. Thus
 * there are possibly different dialing statuses to each end point. The dialing statuses are listed in
 * order of priority and the dialing status that is reported for a user is for the end point that is closest
 * to having a completed a call.
 */
typedef NS_ENUM(NSInteger, VSeeDialingStatus) {
    VSeeDialingStatusUnknown,           ///< Dialing status is unknown or initializing.
    VSeeDialingStatusEnded,             ///< The call has been answered or otherwise ended, such as being cancelled by the local user.
    VSeeDialingStatusOffline,           ///< The remote users is offline.
    VSeeDialingStatusFailed,            ///< The call has been declined or failed.
    VSeeDialingStatusDialing           ///< The call is either looking up or ringing the remote user.
};


@protocol VSeeNotificationCenterDelegate;

/** Class to manage queing and displaying call and chat notifications while app is in the foreground
 *
 * @details A VSeeNotificationCenter object provides a mechanism for queuing chat and call notifications
 * and displaying them one at a time in order of priority. While the app is in the foreground the
 * notifications are displayed by the VSeeNotificationCenter delegate. When the app is in the background
 * UNNotificationRequest objects should be used to notify the user of these events.
 * @ingroup iOSFramework
 */
@interface VSeeNotificationCenter : NSObject

@property (weak, nullable, nonatomic) id<VSeeNotificationCenterDelegate> delegate;  ///< notification center's delegate

+ (instancetype)defaultCenter;      ///< The current process's default notification center

/** Handle a VoIP Push Payload Dictionary
 * @param pushDictionary A dictionary with the contents of the PKPushPayload
 * @return Returns true if the push notification was handled by VSeeKit. For application specific push notifications this will return false.
 * @details You should call this method from the `pushRegistry:didReceiveIncomingPushWithPayload:forType:withCompletionHandler:' method of the
 * application delegate.
 */
- (BOOL)handlePushDictionary:(NSDictionary *)pushDictionary;

/** Handle user response to a notification
 * @param response A user's respone to a notification that may have been posted due to a VSeeKit push notification.
 * @return Returns true if the notification response was handled by VSeeKit. For application specific notifications this will return false.
 * @details You should call this method from the `userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler:` method of the
 * notification delegate.
 */
- (BOOL)handleNotificationResponse:(UNNotificationResponse *)response;

@end


/** Protocol for the VSeeNotificationCenter delegate
 * @details The delegate is responsible for actually displaying the notification views.
 * @ingroup iOSFramework
 */
@protocol VSeeNotificationCenterDelegate<NSObject>

/** Display a notification view controller
 * @param center the notification center
 * @param viewController the view controller to display, which contains the call or chat notification
 * @param notificationType the type of notification to display
 */
- (void)notificationCenter:(VSeeNotificationCenter *)center postNotificationViewController:(UIViewController *)viewController notificationType:(VSeeNotificationType)notificationType;

/** Remove a notification view controller
 * @param center the notification center
 * @param viewController the view controller to remove whose notification has expired
 */
- (void)notificationCenter:(VSeeNotificationCenter *)center removeNotificationViewController:(UIViewController *)viewController;

@optional

/** Notification that the dialing status of an outgoing call has changed
 * @param center the notification center
 * @param dialingStatus the new dialing status for the remote user being called
 * @param message a localized message the describes the dialing status
 * @param accountName the account name of the remote user being called
 * @param displayName the real name of the remote user suitable for display in the UI
 */
- (void)notificationCenter:(VSeeNotificationCenter *)center dialingStatus:(VSeeDialingStatus)dialingStatus message:(NSString *)message forAccountName:(NSString *)accountName displayName:(NSString *)displayName;

/** Notification that all dialing has been canceled
 * @param center the notification center
 * @details This is called when the local user cancels dialing for all outgoing calls.
 */
- (void)notificationCenterDialingCanceled:(VSeeNotificationCenter *)center;

/** Show contact requests
 * @details Show the list of incoming contact requests. This is sent when the user has tapped on a local notification for
 * a new contact request.
 */
- (void)showContactRequests;

/** Show an alert that the user should wait to receive a call
 * @details The alert should be localized and non-blocking. This is intended for use by the Web API
 * and will generally not be implemented in customer apps.
 */
- (void)showWaitForCallAlert;

/** Show Clinic dashboard
 * @details This is sent when the user has tapped on a local notification for a new patient entering the user's
 * waiting room. This will generally not be implemented in customer apps. */
- (void)showClinicDashboard;

@end

NS_ASSUME_NONNULL_END
