/**
 * @file VSeeAddressBook.h
 * Object containing a model of the data in the user's address book
 *
 * @copyright (c) 2015 VSee Lab, Inc. All rights reserved.
 * @ingroup iOSFramework
 */

#import <Foundation/Foundation.h>

@class VSeeContact;
@class VSeeSubscriptionRequest;

/** Object containing a model of the data in the user's address book
 *
 * @details The address book for a user contains VSee accounts, known as contacts, that the user interacts
 * with regularly. Detailed information on each of the contacts in the user's address book is downloaded
 * on login. In addition the presence status of all the contacts in the address book are updated automatically.
 * The contacts are organized into groups and every contact belongs to one or more groups. A user's address
 * book is persisted on the VSee server so that it is available wherever the user logs in.
 * @ingroup iOSFramework
 */
@interface VSeeAddressBook : NSObject

/** Singleton address book object */
+ (instancetype _Nonnull)sharedAddressBook;

/** The contacts in the address book
 *
 * The dictionary contains all the contacts in the user's address book organized by group. The keys are the
 * names of the groups and for each group the value is a VSeeContact set of type NSSet. */
- (nonnull NSDictionary <NSString *, NSSet <VSeeContact *> *> *)contacts;

/** Add a contact to a group in the user's address book
 *
 * @param accountName The account name of the user to add
 * @param groupName The name of the group the contact should be added to or nil
 * @param message An invite message to send the remote user if they are being added to the address book for the first time.
 * @param completion A block object to be run on the main thread when the add completes. This block has no
 * return value and takes a single Boolean argument that indicates whether the account exists on the VSee server.
 * @details If the group does not exist, it will be created. If groupName is nil the default group for new contacts will
 * be used. If an incoming subscription request has been received for this contact it will automatically be accepted and
 * the subscription request will be removed. */
- (void)addAccountName:(nonnull NSString *)accountName toGroupName:(nullable NSString *)groupName inviteMessage:(nullable NSString *)message completion:(nullable void (^)(BOOL success))completion;

/** Add a contact to a group in the user's address book
 *
 * @param accountName The account name of the user to add
 * @param groupName The name of the group the contact should be added to or nil
 * @param completion A block object to be run on the main thread when the add completes. This block has no
 * return value and takes a single Boolean argument that indicates whether the account exists on the VSee server.
 * @details If the group does not exist, it will be created. If groupName is nil the default
 * group for new contacts will be used.
 * @deprecated Use `addAccountName:toGroupName:inviteMessage:completion:` instead. */
- (void)addAccountName:(nonnull NSString *)accountName toGroupNamed:(nullable NSString *)groupName completion:(nullable void (^)(BOOL success))completion __deprecated_msg("Method deprecated. Use `addAccountName:toGroupName:inviteMessage:completion:` instead.");

/** Remove a contact from a group in the user's address book
 *
 * @param accountName The account name of the user to remove
 * @param groupName The name of the group the contact should be removed from or nil for all groups
 * @details If a contact is removed from every group the subscription to the remote user's status will be canceled. */
- (void)removeAccountName:(nonnull NSString *)accountName fromGroupNamed:(nullable NSString *)groupName;

/** Lookup information about a contact
 *
 * @param accountName The account name of the user to lookup
 * @param completion A block object to be run on the main thread when the lookup completes. This block has
 * no return value and takes a single argument of the contact object with all the information retrieved from
 * the VSee server.
 * @details If the account name is not valid the contact will be nil. If the account information is already
 * in the user's address book, this function will return immediately. If the account was not already present,
 * this method adds the account to the user's address book as a temporary contact. This addition only lasts
 * until the user logs out and is not persisted to the VSee server. Status observers registered with
 * addStatusObserver:context: will not receive notifications for temporary contacts because the contacts have
 * not been registered with the server. */
- (void)lookupAccountName:(nonnull NSString *)accountName completion:(nonnull void (^)(VSeeContact * _Nullable contact))completion;

/** Check if a contact is in the address book and create one if not
 *
 * @param accountName The account name to search for in the address book
 * @param exist If this parameter is not NULL it will contain whether or not the contact existed beforehand upon return.
 * This will also be false if the contact found is a temporary contact.
 * @return If the address book contains a contact with the specified user name regardless of case then it will be returned,
 * otherwise a new placeholder contact instance will be created and returned. The placeholder contact will not be added to
 * the address book and will only contain the account name, but no other information.
 * @details Account names for users that have sent chats or calls will already be in the address book. This method provides
 * a synchronous alternative to `lookupAccountName:completion:` when a contact is known to have already been looked up.
 * @remarks Two contacts are considered equal if their user names are the same ignoring case. */
- (nonnull VSeeContact *)contactNamed:(nonnull NSString *)accountName exists:(nullable BOOL *)exist;

/** Register to receive status change notifications for all contacts in the user's address book
 *
 * @param observer The object to receive status change notifications. The object must implement the key
 * value observing method observeValueForKeyPath:ofObject:change:context:.
 * @param context Arbitrary data that is passed to observer in observeValueForKeyPath:ofObject:change:context:.
 * @note The observer will receive status change notifications even for contacts added later to the address book.
 * The status change notifications will always be delivered on the main thread. */
- (void)addStatusObserver:(nonnull NSObject *)observer context:(nullable void *)context;

/** Stops an object from receiving status change notifications for contacts in the user's address book
 *
 * @param observer The object to remove as an observer.
 * @param context Arbitrary data that specifically indentifies the observer to be removed. */
- (void)removeStatusObserver:(nonnull NSObject *)observer context:(nullable void *)context;

/** Decline and remove an incoming subscription request
 * @param accountName The account name of the remote user who made the subscription request.
 * @param markSpam True if all future requests from this account should be automatically declined.
 * @note To accept a subscription request add the requesting account to the address book with
 * `addAccountName:toGroupName:inviteMessage:completion:`. */
- (void)declineSubscriptionRequest:(nonnull NSString *)accountName markAsSpam:(BOOL)markSpam;

/** A set containing all of the incoming subscription requests
 * @details Use `addAccountName:toGroupName:inviteMessage:completion:` to accept subscription
 * requests and `declineSubscriptionRequest:markAsSpam:` to decline them. */
@property (strong, nonnull, nonatomic) NSOrderedSet <VSeeSubscriptionRequest *> *subscriptionRequests;

@end
