/**
 * @file VSeeChatViewControllerProtocol.h
 * Protocol for chat view controllers
 *
 * @copyright (c) 2014 VSee Lab, Inc. All rights reserved.
 * @ingroup iOSFramework
 */

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, VSeeChatType) {
    VSeeChatUnknown,        ///< Unknown chat (what will be unarchived for old archives missing "chatType"
    VSeeChatOneToOne,       ///< One to One chat
    VSeeChatGroup,          ///< Group chat (not in-call chat)
    VSeeChatInCall,         ///< In-call chat
    VSeeChatWaitingRoom     ///< Waiting Room messages (VSee client only)
};


/** Protocol that all chat view controllers must conform to
 * @ingroup iOSFramework
 */
@protocol VSeeChatViewControllerProtocol <NSObject>

/** A unique id for this chat controller
 * @details This is the room name for group chats and the VSee account name for one to one chats. */
@property (strong, readonly, nonnull, nonatomic) NSString *chatId;

@property (assign, readonly, nonatomic) VSeeChatType chatType;          ///< The type of chat session
@property (copy, readonly, nullable, nonatomic) NSString *title;        ///< Localized title to display for the chat
@property (assign, nonatomic) NSUInteger unreadChatCount;               ///< The number of chat messages that are unread
@property (strong, readonly, nonnull, nonatomic) NSArray *participants; ///< All the active participants in the chat excluding the local user
@property (copy, readonly, nullable, nonatomic) NSString *lastSender;   ///< The account name of the last user to send a chat message
@property (copy, readonly, nullable, nonatomic) NSString *lastMessage;  ///< The text of the last chat message received
@property (strong, readonly, nullable, nonatomic) NSDate *lastChatDate; ///< The date and time the last chat message was received
@property (assign, readonly, getter=isActive, nonatomic) BOOL active;   ///< For group chats, whether the user is still in the chat room
@property (assign, nonatomic) NSUInteger priorityChatCount;             ///< Number of un-acknowledged priortyMessages
@property (assign, nonatomic, getter=isChatInputEnabled) BOOL chatInputEnabled;   ///< Enable/Disable status for chat input toolbar

/** A toolbar that appears on the left side of the chat view's top bar
 * @details By default this toolbar is empty but it can be configured to show bar button items specific to the app.
 * @note This toolbar is currenlty only visible on iPad.
 */
@property (strong, nullable, nonatomic) IBOutlet UIToolbar *leftToolbar;

/** Close this chat view controller
 * @details For group chat controllers it will also cause the user to leave the group chat.
 */
- (void)closeChat;

/** Send an outgoing text message in chat view controller
 * @param message the outgoing message to send
 */
- (void)sendMessage:(nonnull NSString *)message;

/** Send an outgoing image message in chat view controller
 * @param image the outgoing image to send
 */
- (void)sendImage:(nonnull UIImage *)image;

/// Send an outgoing file message in chat view controller
/// @param mediaURL The local file URL for the file to send
- (void)sendFile:(nonnull NSURL *)fileURL;

/** Add bar button item to right navigation item
 * @param buttonItem New bar button item to add to right navigation item
 * @details This allows adding addition right bar button items that appear when this chat view controller
 * is displayed in a navigation controller by the chat view manager. Each new bar button item will appear
 * to the right of the existing bar button items.
 * @seealso removeRightBarButtonItem:
 */
- (void)addRightBarButtonItem:(nonnull UIBarButtonItem *)buttonItem;

/** Remove bar button item from right navigation item
 * @param buttonItem Bar button item to remove from right navigation item
 * @seealso addRightBarButtonItem:
 */
- (void)removeRightBarButtonItem:(nonnull UIBarButtonItem *)buttonItem;

/** Specifies an inset for the chat view's frame
 * @param inset The edge inset values to shrink or expand the chat view's frame.
 * @details Use this property to adjust the inset to account for a custom subview at the top of your view controller.
 * @note Currently only the top edge of the inset is used.
 */
- (void)adjustMessageViewContentInset:(UIEdgeInsets)inset;

@end
