//
//  VSeeKit.h
//  VSeeKit
//
//  Created by Torrey Lyons on 9/19/14.
//  Copyright (c) 2014 VSee Lab, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VSeeKit.
FOUNDATION_EXPORT const double VSeeKitVersionNumber;

//! Project version string for VSeeKit.
FOUNDATION_EXPORT const unsigned char VSeeKitVersionString[];

#import <VSeeKit/VSeeAddressBook.h>
#import <VSeeKit/VSeeAVCapture.h>
#import <VSeeKit/VSeeChatArchive.h>
#import <VSeeKit/VSeeChatMessage.h>
#import <VSeeKit/VSeeChatService.h>
#import <VSeeKit/VSeeChatViewControllerProtocol.h>
#import <VSeeKit/VSeeChatViewManager.h>
#import <VSeeKit/VSeeContact.h>
#import <VSeeKit/VSeeNavigationController.h>
#import <VSeeKit/VSeeNotificationCenter.h>
#import <VSeeKit/VSeePhotoMediaProtocol.h>
#import <VSeeKit/VSeeServerConnection.h>
#import <VSeeKit/VSeeStatusHelper.h>
#import <VSeeKit/VSeeTabBarChatViewManager.h>
#import <VSeeKit/VSeeVideoViewManager.h>
#import <VSeeKit/VSeeWaitingRoom.h>
#import <VSeeKit/VSeeWindow.h>
