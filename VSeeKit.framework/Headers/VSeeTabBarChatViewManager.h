/**
 * @file VSeeTabBarChatViewManager.h
 * Singleton view controller of the top level chat UI with a tab bar for switching chats
 *
 * @details This subclass can be used as an alternative to VSeeChatViewManager. Its major
 * difference is that it incorporates a tab bar at the top of the screen listing all of
 * the recent chats. This allow the user to between which recent chat to show or to hide
 * an individual chat and remove it from the recent chat list. To use this chat view
 * manager instead of VSeeChatViewManager, create the shared instance of this class before
 * login as follows:
 *      [VSeeTabBarChatViewManager sharedManager];
 * @note The tab bar chat view manager is currently only supported on iPad.
 * @ingroup iOSUI
 */

#import <UIKit/UIKit.h>
#import "VSeeChatViewManager.h"

/** Height of the chat tab in points */
extern const CGFloat VSeeChatTabHeight;

/**
 * Singleton view controller of the top level chat UI with a tab bar for switching chats
 *
 * @details This subclass can be used as an alternative to VSeeChatViewManager. Its major
 * difference is that it incorporates a tab bar at the top of the screen listing all of
 * the recent chats. This allow the user to between which recent chat to show or to hide
 * an individual chat and remove it from the recent chat list. To use this chat view
 * manager instead of VSeeChatViewManager, create the shared instance of this class before
 * login as follows:
 *      [VSeeTabBarChatViewManager sharedManager];
 * @note The tab bar chat view manager is currently only supported on iPad.
 * @ingroup iOSUI
 */
@interface VSeeTabBarChatViewManager : VSeeChatViewManager
@end
