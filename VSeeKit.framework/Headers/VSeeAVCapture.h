/**
 * @file VSeeAVCapture.h
 * Class to control audio and video capture
 *
 * @copyright (c) 2015 VSee Lab, Inc. All rights reserved.
 * @ingroup iOSFramework
 */

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

/** Class to control audio and video capture
 *
 * @details A VSeeAVCapture object provides an interface to control audio and video capture by VSeeKit.
 * @ingroup iOSFramework
 */
@interface VSeeAVCapture : NSObject <AVCaptureVideoDataOutputSampleBufferDelegate>

/** Allows enabling stethoscope audio mode
 * @details Default is NO. Stethoscope audio mode is not recommended for general users. It is only intended
 * for stethoscopes and will result in higher bandwidth usage and poor speech clarity.
 * @note Stethoscope audio is experimental and subject to change. */
@property (assign, nonatomic) BOOL stethoscopeAudioMode;

/** Defines if an external mic is assumed to be a stethoscope. If so, then special stethoscope audio mode will be used
 * whenever external mic is the active input device.
 * If a user does NOT want a remote user to be able to change between internal and external mic, then set this to FALSE
 * @details Default is NO */
@property (assign, nonatomic) BOOL externalMicIsStethoscope;

/** Disable all video capture
 * @details The default is NO. This should be set before login and should not be changed while logged in.
 * @note Disabling video capture is not the same as muting video. */
@property (assign, nonatomic) BOOL disableCamera;

/** The video sample buffer delegate for preprocessing
 * @details When a new video sample buffer is captured it is sent to the video sample buffer delegate using
 * captureOutput:didOutputSampleBuffer:fromConnection:. All delegate methods are invoked on VSeeKit's
 * dispatch queue for video processing. When processing of a video sample buffer is complete the modified
 * video sample buffer should be sent on to VSeeKit using captureOutput:didOutputSampleBuffer:fromConnection:
 * on the videoOutputDelegate object. The sample buffer sent to the video output delegate must be in pixel format
 * kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange or kCVPixelFormatType_32BGRA. */
@property (weak, nullable, nonatomic) id <AVCaptureVideoDataOutputSampleBufferDelegate> videoSampleBufferDelegate;

/** One or more pixel format types used for the video sample buffer
 * @details This may be a single CFNumber value for a Core Video pixel format type or a CFArray containing
 * multiple CFNumber values. A list of Core Video pixel format types can be found in CVPixelBuffer.h. This
 * property is only used if videoSampleBufferDelegate is non-nil. No matter in which format the video sample
 * buffer delegate receives the video data, the video sample buffer must be sent on to the VSeeAVCapture
 * object in pixel format kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange or kCVPixelFormatType_32BGRA.
 */
@property (strong, nullable, nonatomic) id videoPixelFormatType;

/** Rotate video sample buffers to a specific orientation 
 */
@property(assign, nonatomic) AVCaptureVideoOrientation videoOrientation;

/** The video output delegate to receive preprocessed video sample buffers
 * @details When a video sample buffer has been processed the videoSampleBufferDelegate must callback this
 * object using captureOutput:didOutputSampleBuffer:fromConnection: to pass the video frame onto VSeeKit.
 * The sample buffer sent to this video output delegate must be in pixel format
 * kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange or kCVPixelFormatType_32BGRA.
 * @note This can change at any time as the user switches between front and back cameras. However, all changes
 * are synchronized with VSeeKit's dispatch queue for video processing.
 */
@property (weak, readonly, nullable, nonatomic) id <AVCaptureVideoDataOutputSampleBufferDelegate> videoOutputDelegate;

/** Size of the video output used by VSeeKit
 * @details VSeeKit will try to match the video resolution captured by the camera to this size, but the
 * actual camera resolution may be larger.
 */
@property (assign, readonly) CGSize videoSize;

+ (instancetype)sharedAVCapture;        ///< Singleton A/V Capture object

@end

NS_ASSUME_NONNULL_END
