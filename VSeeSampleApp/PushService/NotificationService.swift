//
//  NotificationService.swift
//  PushService
//
//  Created by Torrey Lyons on 6/1/20.
//  Copyright © 2020 Vsee Lab, Inc. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice, this
//  list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)

        if let bestAttemptContent = bestAttemptContent {
            var userInfo = bestAttemptContent.userInfo

            // Provide the generic notification for each category
            if let category = userInfo["category"] as? String {
                let priority = userInfo["priority"] as? Bool ?? false
                if priority {
                    bestAttemptContent.sound = UNNotificationSound.init(named: UNNotificationSoundName("VSeeKit.bundle/pm_notification.wav"))
                } else {
                    bestAttemptContent.sound = UNNotificationSound.init(named: UNNotificationSoundName("VSeeKit.bundle/chat-a.wav"))
                }

                switch category {
                case oneToOneChatCategory:
                    bestAttemptContent.title = NSLocalizedString("You have a new message.", comment: "")
                    userInfo["chatType"] = 1
                case groupInviteCategory:
                    bestAttemptContent.title = NSLocalizedString("You have been invited to a group chat.", comment: "")
                case groupChatCategory:
                    bestAttemptContent.title = NSLocalizedString("You have a new group chat message.", comment: "")
                    userInfo["chatType"] = 2
                case waitingRoomCategory:
                    bestAttemptContent.title = NSLocalizedString("A user has entered your waiting room.", comment: "")
                    userInfo["chatType"] = 4
                    bestAttemptContent.sound = UNNotificationSound.init(named: UNNotificationSoundName("VSeeKit.bundle/String-sounds-938.wav"))
                case newUserOnlineCategory:
                    bestAttemptContent.title = NSLocalizedString("Your invitee has come online.", comment: "")
                case systemCategory:
                    bestAttemptContent.title = NSLocalizedString("You have a new message", comment: "")
                    userInfo["chatType"] = 1
                case videoCallCategory:
                    bestAttemptContent.title = NSLocalizedString("Incoming video call", comment: "")
                    bestAttemptContent.sound = UNNotificationSound.init(named: UNNotificationSoundName("VSeeKit.bundle/ring1x.wav"))
                case contactRequestCategory:
                    bestAttemptContent.title = NSLocalizedString("You have received a new contact request.", comment: "")
                default:
                    bestAttemptContent.sound = UNNotificationSound.default
                }

                if priority {
                    bestAttemptContent.title = "[PRIORITY]" + bestAttemptContent.title
                }
            }

            bestAttemptContent.userInfo = userInfo
            contentHandler(bestAttemptContent)
        }
    }

    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}
