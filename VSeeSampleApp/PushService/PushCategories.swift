//
//  PushCategories.swift
//  PushService
//
//  Created by Torrey Lyons on 11/22/19.
//  Copyright © 2019 VSee Lab, Inc. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice, this
//  list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import Foundation

let oneToOneChatCategory = "VSeeChatOneToOne"
let groupInviteCategory = "VSeeChatInvite"
let groupChatCategory = "VSeeChatGroup"
let waitingRoomCategory = "VSeeChatWaitingRoom"
let newUserOnlineCategory = "VSeeChatNewUserOnline"
let systemCategory = "VSeeChatSystem"
let videoCallCategory = "VSeeVideoCall"
let contactRequestCategory = "VSeeContactRequest"
let keepAliveCategory = "VSeeKeepAlive"

enum ChatType: Int {
    case unknown = 0        ///< Unknown chat (what will be unarchived for old archives missing "chatType"
    case oneToOne = 1       ///< One to One chat
    case group = 2          ///< Group chat (not in-call chat)
    case inCall = 3         ///< In-call chat
    case waitingRoom = 4    ///< Waiting Room messages (VSee client only)
}
