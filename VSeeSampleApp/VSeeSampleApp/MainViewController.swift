//
//  MainViewController.swift
//  VSeeSampleApp
//
//  Created by Ken Tran on 13/3/17.
//  Copyright © 2017 Vsee Lab, Inc. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice, this
//  list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import Foundation
import VSeeKit

class MainViewController: UITableViewController {
    
    let CellIdentifier = "Cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
        self.navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath)
        
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "Address book"
        case 1:
            cell.textLabel?.text = "Recent chats"
        case 2:
            cell.textLabel?.text = "Switch to video"
        case 3:
            cell.textLabel?.text = "Call contact"
        case 4:
            cell.textLabel?.text = "Chat with contact"
        case 5:
            cell.textLabel?.text = "Send chat directly"
        case 6:
            cell.textLabel?.text = "Log out"
        default:
            break
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.showAddressBook()
        case 1:
            self.showRecentChats()
        case 2:
            self.showLocalVideo()
        case 3:
            self.callContact()
        case 4:
            self.chatWithContact()
        case 5:
            self.sendChatToContactDirectly()
        case 6:
            self.logout()
        default:
            break
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Action
    
    func showAddressBook() {
        let addressBookController = AddressBookViewController()
        self.navigationController?.pushViewController(addressBookController, animated: true)
    }
    
    func showRecentChats() {
        let recentChatController = RecentChatViewController()
        self.navigationController?.pushViewController(recentChatController, animated: true)
    }
    
    func logout() {
        self.navigationController?.popViewController(animated: true)
        VSeeServerConnection.default().logout()
    }
    
    func showLocalVideo() {
        CallManager.sharedInstance.showVideoView()
    }
    
    func callContact() {
        let alertController = UIAlertController(title: nil, message: "Call", preferredStyle: .alert)
        let enterAction = UIAlertAction(title: "OK", style: .default) { (action) in
            let username = alertController.textFields?.first?.text
            if username != nil {
                VSeeVideoViewManager.shared().startVideoCall(withUserNames: [username!])
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(enterAction)
        alertController.addAction(cancelAction)
        alertController.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Username"
        })
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func chatWithContact() {
        let alertController = UIAlertController(title: nil, message: "Chat", preferredStyle: .alert)
        let enterAction = UIAlertAction(title: "OK", style: .default) { (action) in
            let username = alertController.textFields?.first?.text
            if username != nil {
                VSeeChatViewManager.shared().showOneToOneChat(withUserName: username!)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(enterAction)
        alertController.addAction(cancelAction)
        alertController.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Username"
        })
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sendChatToContactDirectly() {
        let alertController = UIAlertController(title: nil, message: "Chat", preferredStyle: .alert)
        let enterAction = UIAlertAction(title: "OK", style: .default) { (action) in
            let username = alertController.textFields?.first?.text
            if username != nil {
                VSeeChatViewManager.shared().showOneToOneChat(withUserName: username!)
                let controller = VSeeChatViewManager.shared().chatController(forUserName: username!)! as UIViewController & VSeeChatViewControllerProtocol
                controller.sendMessage("Test Chat Message");
                
                let url = "https://img.purch.com/w/660/aHR0cDovL3d3dy5zcGFjZS5jb20vaW1hZ2VzL2kvMDAwLzA3NS83MTgvb3JpZ2luYWwvc3BhY2V4LWZhbGNvbi1oZWF2eS1ib29zdGVycy1sYW5kLmpwZw=="
                let forecastURL = NSURL(string: url)
                let myImage = NSData (contentsOf: forecastURL! as URL)
                let image = UIImage(data: myImage! as Data)
                
                controller.send(image!);
                
                // Alternatively, while not recommended, if there is a requirement to prevent the chat view from showing, you can send chat messages bypassing the UI.
                // The downside of this method is that you will not be able to see these messages in a local chat view until the client is logged in again.
                // for sending images in this way, you'll also have to take a few more steps, including uploading the media and waiting for a callback.

                /****
                let chatMessage = VSeeOneToOneChatMessage()
                chatMessage.message = "test"
                chatMessage.recipientId = username!;
                chatMessage.sendMessage();
                ****/

            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(enterAction)
        alertController.addAction(cancelAction)
        alertController.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Username"
        })
        
        self.present(alertController, animated: true, completion: nil)
    }
}
