//
//  CallManager.swift
//  VSeeSampleApp
//
//  Created by Ruozhen on 10/9/14.
//  Copyright (c) 2014 VSee Lab, Inc. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice, this
//  list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import Foundation
import VSeeKit

private let _CallManagerSharedInstance = CallManager()

class CallManager: NSObject, VSeeChatViewManagerDelegate, VSeeVideoViewManagerDelegate, VSeeNotificationCenterDelegate, AVCaptureVideoDataOutputSampleBufferDelegate {
    var notificationViewController: UIViewController?
    var showingVideo: Bool = false
    var navigationController: VSeeNavigationController?
    var waitForCallController: WaitForCallViewController = WaitForCallViewController(nibName: "WaitForCallView", bundle: nil)

    class var sharedInstance: CallManager {
        return _CallManagerSharedInstance
    }

    override init() {
        super.init()
        // setup video/chat/notification delegate
        VSeeVideoViewManager.shared().delegate = self
        VSeeChatViewManager.shared().delegate = self
        VSeeNotificationCenter.default().delegate = self

        VSeeVideoViewManager.shared().waitForCallOffset = UIOffset.init(horizontal: 0.0, vertical: (205.0 - 64.0)/2.0)
    }

    // helper functions
    func showManagerWindow(_ manager:UIViewController) {
        var exist_manager:Bool = false
        let isVisibleViewController:Bool = manager.navigationController?.visibleViewController == manager

        if let navigationController = manager.navigationController {
            let controllers = navigationController.viewControllers
            for controller in controllers {
                if controller == manager {
                    exist_manager = true
                }
            }
        }
        if !exist_manager {
            self.navigationController!.pushViewController(manager, animated: true)
        }
        else if !isVisibleViewController {
            self.navigationController!.popToViewController(manager, animated: true)
        }
    }

    func showChatView() {
        let manager:VSeeChatViewManager = VSeeChatViewManager.shared()
        self.navigationController!.isNavigationBarHidden = false
        self.showManagerWindow(manager)
    }

    func showVideoView() {
        let manager:VSeeVideoViewManager = VSeeVideoViewManager.shared()
        manager.navigationItem.hidesBackButton = true
        self.navigationController!.isNavigationBarHidden = true

        if (!self.showingVideo) {
            manager.waitForCallViewController = self.waitForCallController
            self.showingVideo = true
        }

        self.showManagerWindow(manager)
    }

    func endVideoCall() {
        if self.navigationController!.viewControllers.contains(VSeeVideoViewManager.shared()) {
            self.navigationController?.popViewController(animated: true)
        }
    }

    // MARK: - Video View Manager Delegate
    // Used to show video view
    func videoViewManagerShowVideo(_ manager: VSeeVideoViewManager) {
        self.showVideoView()
    }

    func videoViewManager(_ manager: VSeeVideoViewManager, didRemoveUser accountName: String, isLastRemote last: Bool) {
        if (last) {
            self.showingVideo = false
            endVideoCall()
        }
    }

    func videoViewManager(_ manager: VSeeVideoViewManager, willShowUser accountName: String, isRemote remote: Bool) {
        if (manager.isInVideoCall) {
            manager.waitForCallViewController = nil
        }
        showVideoView()
    }

    func chatSelected(_ sender: UIBarButtonItem) {
        self.chatViewManagerShowChat(VSeeChatViewManager.shared())
    }

    // MARK: - Chat View Manager Delegate
    // Used to show chat view

    func chatViewManager(_ manager: VSeeChatViewManager, didAddFirstChat addFirst: Bool) {

    }

    func chatViewManager(_ manager: VSeeChatViewManager, didRemoveLastChat last: Bool) {

    }

    func chatViewManagerHideChat(_ manager: VSeeChatViewManager) {
        self.navigationController!.popToRootViewController(animated: true)
    }

    func chatViewManagerShowChat(_ manager: VSeeChatViewManager) {
        self.showChatView()
    }

    // MARK: - Notification Center Delegate
    // Used to show notification messages

    func notificationCenter(_ center: VSeeNotificationCenter, postNotificationViewController viewController: UIViewController, notificationType: VSeeNotificationType) {
        if notificationType == .VSeeNotificationCall {
            viewController.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 150.0, width: UIScreen.main.bounds.width, height: 150.0)
        } else {
            viewController.view.frame = CGRect(x: 0, y: 66.0, width: UIScreen.main.bounds.size.width, height: 64.0)
        }
        UIApplication.shared.keyWindow!.addSubview(viewController.view)
        self.notificationViewController = viewController;
    }

    func notificationCenter(_ center: VSeeNotificationCenter, removeNotificationViewController viewController: UIViewController) {
        if notificationViewController != nil {
            viewController.view.removeFromSuperview()
            self.notificationViewController = nil
        }
    }

    func notificationCenter(_ center: VSeeNotificationCenter, dialingStatus: VSeeDialingStatus, message: String, forAccountName accountName: String, displayName: String) {
        if dialingStatus == .failed || dialingStatus == .offline {
            let alertController = UIAlertController(title: "Message", message: "\(#function): dialing status - \(dialingStatus.rawValue)", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }

    // MARK: -  AV Capture Video Sample Buffer Delegate

    func captureOutput(_ captureOutput: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        VSeeAVCapture.shared().videoOutputDelegate!.captureOutput!(captureOutput, didOutput: sampleBuffer, from: connection)
    }

}
