//
//  LoginViewController.swift
//  VSeeSampleApp
//
//  Created by Ruozhen on 10/9/14.
//  Copyright (c) 2014 VSee Lab, Inc. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice, this
//  list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import UIKit
import VSeeKit

class LoginViewController: UIViewController, VSeeServerConnectionDelegate {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var loggedInLabel: UILabel!
    @IBOutlet weak var loginActivityIndicatorView: UIActivityIndicatorView!

    var inprogress: Bool = false
    let userIdKey = "userID"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup the VSeeServerConnection delegate
        VSeeServerConnection.default().delegate = self

        if let userId = UserDefaults.standard.object(forKey: userIdKey) as? String {
            self.usernameTextField.text = userId
        }
        self.loginButton.isEnabled = false

        VSeeServerConnection.validateKey(apiKey, secretHash: secretHash) { (valid, error) in
            if valid {
                self.loginButton.isEnabled = true
                VSeeServerConnection.default().loginRememberedUser()
            } else {
                print("Unable to validate API key")
                print(error!)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Login to VSee directory
    // Use VSeeServerConnection delegate to keep track of the connection status
    @IBAction func loginButtonClicked(_ sender: AnyObject) {
        UserDefaults.standard.set(usernameTextField.text, forKey: userIdKey)
        VSeeServerConnection.default().loginUser(usernameTextField.text!, password: passwordTextField.text!)
        loginActivityIndicatorView.startAnimating()
        self.loginButton.isEnabled = false
        self.loginButton.isHidden = true
        self.usernameTextField.isEnabled = false
        self.passwordTextField.isEnabled = false
    }

    // pragma mark - VSeeServerConnection Delegate and helper function
    // Keep track of the connection status

    func updateUIForLoginState(_ state: VSeeLoginState) {
        switch (state) {
        case VSeeLoginState.initializing:
            print("updateUIForLoginState: Initializing")
        case VSeeLoginState.connecting:
            print("updateUIForLoginState: Connecting")
        case VSeeLoginState.idle:
            print("updateUIForLoginState: Idle")
            if self.inprogress {
                self.loginButton.isHidden = false
                self.loggedInLabel.text = ""
                self.loginButton.isEnabled = true
                self.usernameTextField.isEnabled = true
                self.passwordTextField.isEnabled = true
                loginActivityIndicatorView.stopAnimating()
                self.inprogress = false
            }
        case VSeeLoginState.tryLogin:
            print("updateUIForLoginState: TryLogin")
            self.inprogress = true
        case VSeeLoginState.loggedIn:
            if self.inprogress {
                self.inprogress = false
                
                self.loginButton.isHidden = true
                loginActivityIndicatorView.stopAnimating()
                self.loggedInLabel.isHidden = false
                self.loggedInLabel.text = "Logged in as " + self.usernameTextField.text!
                
                let mainViewController = MainViewController(style: .grouped)
                self.navigationController?.pushViewController(mainViewController, animated: true)
            }

        case VSeeLoginState.tryLogout:
            print("updateUIForLoginState: TryLogout")
            self.loggedInLabel.text = "Logging out"
            self.inprogress = true
            loginActivityIndicatorView.startAnimating()
        default:
            print("Unknow Login State")
        }
    }

    func serverConnection(_ connection: VSeeServerConnection, loginState: VSeeLoginState) {
        updateUIForLoginState(loginState)
    }

    func serverConnection(_ connection: VSeeServerConnection, message: String) {
        messageLabel.text = message
    }

}
