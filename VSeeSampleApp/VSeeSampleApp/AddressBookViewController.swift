//
//  AddressBookViewController.swift
//  VSeeSampleApp
//
//  Created by Ken Tran on 13/3/17.
//  Copyright © 2017 Vsee Lab, Inc. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice, this
//  list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import Foundation
import VSeeKit

class AddressBookViewController: UITableViewController {

    let CellIdentifier = "ContactCell"
    
    var contacts = [VSeeContact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure table view
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier)
        self.tableView.allowsMultipleSelection = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Call/Chat", style: .plain, target: self, action: #selector(callChatAction))
        
        // Load contacts from address book
        let contactsDict = VSeeAddressBook.shared().contacts()
        for key in contactsDict.keys {
            let contactsInGroup = (contactsDict[key]! as NSSet).allObjects as! [VSeeContact]
            self.contacts.append(contentsOf: contactsInGroup)
        }
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = false
    }
    
    var selectedContacts: [VSeeContact] {
        var selectedContacts = [VSeeContact]()
        for indexPath in self.tableView.indexPathsForSelectedRows! {
            selectedContacts.append(self.contacts[indexPath.row])
        }
        return selectedContacts
    }
    
    @objc func callChatAction() {
        if self.tableView.indexPathsForSelectedRows != nil {
            let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let callAction = UIAlertAction(title: "Call", style: .default, handler: { (action) in
                self.call(contacts: self.selectedContacts)
            })
            let chatAction = UIAlertAction(title: "Chat", style: .default, handler: { (action) in
                self.chat(contacts: self.selectedContacts)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            actionSheetController.addAction(callAction)
            actionSheetController.addAction(chatAction)
            actionSheetController.addAction(cancelAction)
            
            self.present(actionSheetController, animated: true, completion: nil)
        }
    }
    
    func call(contacts: [VSeeContact]) {
        let usernames = contacts.map({ $0.userName })
        VSeeVideoViewManager.shared().startVideoCall(withUserNames: usernames)
    }
    
    func chat(contacts: [VSeeContact]) {
        let usernames = contacts.map({ $0.userName })
        if usernames.count == 1 {
            // If only 1 contact selected, show 1-1 chat
            VSeeChatViewManager.shared().showOneToOneChat(withUserName: usernames.first!)
        } else {
            // If multiple contact selected, show group chat
            VSeeChatViewManager.shared().showGroupChat(withUserNames: usernames)
        }
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: CellIdentifier)
        
        let contact = self.contacts[indexPath.row]
        cell.textLabel?.text = (contact.fullName != nil && contact.fullName!.length > 0) ? contact.fullName as String? : contact.userName
        switch contact.status {
        case .logout:
            cell.detailTextLabel?.text = "Offline"
        case .login:
            cell.detailTextLabel?.text = "Online"
        case .away:
            cell.detailTextLabel?.text = "Away"
        case .idle:
            cell.detailTextLabel?.text = "Idle"
        case .onCall:
            cell.detailTextLabel?.text = "On call"
        case .busy:
            cell.detailTextLabel?.text = "Busy"
        default:
            cell.detailTextLabel?.text = "Other"
        }
        
        return cell
    }
}
