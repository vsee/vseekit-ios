//
//  RecentChatViewController.swift
//  VSeeSampleApp
//
//  Created by Ken Tran on 15/3/17.
//  Copyright © 2017 Vsee Lab, Inc. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice, this
//  list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import Foundation
import VSeeKit

class RecentChatViewController: UITableViewController {
    
    let CellIdentifier = "Chat Cell"
    
    var chatControllers = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier)
        self.chatControllers = VSeeChatViewManager.shared().chatControllers
        VSeeNotificationCenter.default().delegate = self
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatControllers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: CellIdentifier)
        
        let chatController = self.chatControllers[indexPath.row] as! VSeeChatViewControllerProtocol
        cell.textLabel?.text = (chatController.participants as? [String])?.joined(separator: ", ")
        cell.detailTextLabel?.text = chatController.lastMessage
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatController = self.chatControllers[indexPath.row]
        VSeeChatViewManager.shared().showChatController(chatController as? UIViewController & VSeeChatViewControllerProtocol)
    }
}

extension RecentChatViewController: VSeeNotificationCenterDelegate {
    func notificationCenter(_ center: VSeeNotificationCenter, postNotificationViewController viewController: UIViewController, notificationType: VSeeNotificationType) {
        self.tableView.reloadData()
    }
    
    func notificationCenter(_ center: VSeeNotificationCenter, removeNotificationViewController viewController: UIViewController) {
        
    }
    
}
