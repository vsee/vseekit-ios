# VSeeKit for iOS README #

The VSeeKit iOS Framework allows developers to embed VSee multi-way video calling and chat functionality into their iOS apps. An Android version of VSeeKit is also available with similar features and a Java-based API at [Bintray](https://bintray.com/vsee/public/VSeeKit). The VSeeKit framework, sample code, and documentation are freely available for developers to review. However, to run an app using VSeeKit requires that you obtain an API key from VSee to authorize your use. If you do not have an API key please contact [VSee Sales](https://vsee.com/contactsales) for a free trial API key.

### Getting Started ###

To save space on your local development machine, VSeeKit uses the Git Large File Storage [Git-LFS](https://www.atlassian.com/git/tutorials/git-lfs) extension. To correctly download VSeeKit.framework you will need to use a Git LFS aware client such as the [Git LFS command line client](https://git-lfs.github.com).

For complete details on downloading and setting up VSeeKit in your project, refer to the VSeeKit_Framework.pdf file in the repository.
